package endercrypt.library.commons.listener;


import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;


public class ListenerCollectionTest
{
	@Test
	void testCall()
	{
		ListenerCollection<Cow> cows = new ListenerCollection<>(Cow.class);
		
		Cow expected = mock();
		cows.add(expected);
		
		verify(expected, never()).moo();
		
		cows.getTrigger().moo();
		
		verify(expected, times(1)).moo();
	}
	
	private interface Cow
	{
		public void moo();
	}
}
