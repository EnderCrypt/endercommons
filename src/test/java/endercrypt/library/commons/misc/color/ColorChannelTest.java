package endercrypt.library.commons.misc.color;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.library.commons.misc.color.ColorChannel.Value;

import java.awt.Color;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;


public class ColorChannelTest
{
	@Test
	void getsCorrectIntegerColors()
	{
		Color color = new Color(10, 20, 30, 40);
		
		Map<ColorChannel, Integer> channels = ColorChannel.streamValues(color)
			.collect(Collectors.toMap(ColorChannel.Value::getChannel, ColorChannel.Value::asInteger));
		
		assertEquals(10, channels.get(ColorChannel.RED));
		assertEquals(20, channels.get(ColorChannel.GREEN));
		assertEquals(30, channels.get(ColorChannel.BLUE));
		assertEquals(40, channels.get(ColorChannel.ALPHA));
	}
	
	@Test
	void getsCorrectFloatColors()
	{
		Color color = new Color(0.1f, 0.2f, 0.3f, 0.4f);
		
		Map<ColorChannel, Float> channels = ColorChannel.streamValues(color)
			.collect(Collectors.toMap(ColorChannel.Value::getChannel, ColorChannel.Value::asFloat));
		
		float floatTolerance = 1f / 255;
		assertEquals(0.1f, channels.get(ColorChannel.RED), floatTolerance);
		assertEquals(0.2f, channels.get(ColorChannel.GREEN), floatTolerance);
		assertEquals(0.3f, channels.get(ColorChannel.BLUE), floatTolerance);
		assertEquals(0.4f, channels.get(ColorChannel.ALPHA), floatTolerance);
	}
	
	@Test
	void extractBitColors()
	{
		Color original = new Color(10, 20, 30);
		int color = original.getRGB();
		assertEquals(10, ColorChannel.RED.extract(color));
		assertEquals(20, ColorChannel.GREEN.extract(color));
		assertEquals(30, ColorChannel.BLUE.extract(color));
		assertEquals(255, ColorChannel.ALPHA.extract(color));
	}
	
	@Test
	void extractInduvidualChannel()
	{
		Color original = new Color(10, 20, 30);
		Value value = ColorChannel.GREEN.extractAsValue(original);
		assertEquals(ColorChannel.GREEN.create(20), value);
		
		Value valueFromInt = ColorChannel.GREEN.extractAsValue(original.getRGB());
		assertEquals(value, valueFromInt);
	}
	
	@Test
	void creatingOutOfBoundsColorChannels()
	{
		assertThrows(IllegalArgumentException.class, () -> ColorChannel.GREEN.create(-1));
		assertThrows(IllegalArgumentException.class, () -> ColorChannel.GREEN.create(256));
	}
}
