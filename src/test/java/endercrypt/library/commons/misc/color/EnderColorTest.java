package endercrypt.library.commons.misc.color;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.library.commons.misc.Ender;

import java.awt.Color;

import org.junit.jupiter.api.Test;


public class EnderColorTest
{
	@Test
	void invert()
	{
		assertEquals(new Color(255 - 10, 255 - 20, 255 - 30, 99), Ender.color.invert(new Color(10, 20, 30, 99), false));
		assertEquals(new Color(255 - 10, 255 - 20, 255 - 30, 255 - 99), Ender.color.invert(new Color(10, 20, 30, 99), true));
	}
	
	@Test
	void getColorDifference()
	{
		{
			Color color1 = new Color(20, 50, 70, 1);
			Color color2 = new Color(20, 51, 100, 240);
			assertEquals(31, Ender.color.getColorDifference(color1, color2, false));
			assertEquals(270, Ender.color.getColorDifference(color1, color2, true));
		}
		{
			Color color1 = new Color(40, 30, 20, 10);
			Color color2 = new Color(39, 29, 21, 5);
			assertEquals(3, Ender.color.getColorDifference(color1, color2, false));
			assertEquals(8, Ender.color.getColorDifference(color1, color2, true));
		}
		{
			Color color1 = new Color(10, 20, 30, 40);
			Color color2 = new Color(10, 20, 30, 40);
			assertEquals(0, Ender.color.getColorDifference(color1, color2, false));
			assertEquals(0, Ender.color.getColorDifference(color1, color2, true));
		}
	}
	
	@Test
	void transform()
	{
		Color original = new Color(10, 20, 30, 40);
		
		ColorValueTransformer transformer = (channel, value) -> value + 1;
		
		assertEquals(new Color(11, 21, 31, 40), Ender.color.transform(original, transformer, false));
		assertEquals(new Color(11, 21, 31, 41), Ender.color.transform(original, transformer, true));
	}
}
