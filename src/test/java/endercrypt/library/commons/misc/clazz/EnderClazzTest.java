package endercrypt.library.commons.misc.clazz;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.pubimm.PublicallyImmutableCollection;

import java.awt.Point;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;


public class EnderClazzTest
{
	@Test
	void getCallerClass()
	{
		assertEquals(EnderClazzTest.class, callGetCallerClass(null));
		assertEquals(EnderClazzTest.class, callGetCallerClass(1));
		assertEquals("jdk.internal.reflect.NativeMethodAccessorImpl", callGetCallerClass(2).getName()); // possibly sensitive to runtime context
		assertThrows(IllegalArgumentException.class, () -> callGetCallerClass(0));
		assertThrows(IllegalArgumentException.class, () -> callGetCallerClass(-1));
		assertThrows(IllegalArgumentException.class, () -> callGetCallerClass(Integer.MIN_VALUE));
	}
	
	private Class<?> callGetCallerClass(Integer depth)
	{
		if (depth == null)
		{
			return Ender.clazz.getCallerClass();
		}
		else
		{
			return Ender.clazz.getCallerClass(depth);
		}
	}
	
	@Test
	void getHumanReadableClassName()
	{
		assertEquals("Point", Ender.clazz.getHumanReadableClassName(Point.class));
		assertEquals("Ender Clazz Test", Ender.clazz.getHumanReadableClassName(EnderClazzTest.class));
		assertEquals("Publically Immutable Collection", Ender.clazz.getHumanReadableClassName(PublicallyImmutableCollection.class));
	}
	
	@Test
	void getStackTrace()
	{
		List<StackTraceElement> stack = Ender.clazz.getStackTrace();
		assertEquals("endercrypt.library.commons.misc.clazz.EnderClazzTest", stack.get(0).getClassName());
		assertEquals("getStackTrace", stack.get(0).getMethodName());
		
		assertEquals("jdk.internal.reflect.NativeMethodAccessorImpl", stack.get(1).getClassName());
		assertEquals("invoke0", stack.get(1).getMethodName());
	}
	
	@Test
	void getClassHierarchy()
	{
		Iterator<Class<?>> iterator = Ender.clazz.getClassHierarchy(EnderClazz.class).iterator();
		assertEquals(EnderClazz.class, iterator.next());
		assertEquals(Ender.class, iterator.next());
		assertEquals(Object.class, iterator.next());
		assertFalse(iterator.hasNext());
	}
	
	@Test
	void getClassParents()
	{
		Iterator<Class<?>> iterator = Ender.clazz.getClassParents(EnderClazz.class).iterator();
		assertEquals(Ender.class, iterator.next());
		assertEquals(Object.class, iterator.next());
		assertFalse(iterator.hasNext());
	}
}
