package endercrypt.library.commons.misc.graphics;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.FontMetrics;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor class FontMetricsTester
{
	private final FontMetrics real;
	private final FontMetrics fake;
	
	public void test(String name, FontMetricsMethod method)
	{
		Object realValue = method.call(real);
		Object fakeValue = method.call(fake);
		assertEquals(realValue, fakeValue, "FontMetrics fails for " + name);
	}
}