package endercrypt.library.commons.misc.graphics;

import java.awt.FontMetrics;

interface FontMetricsMethod
{
	public Object call(FontMetrics fontMetrics);
}