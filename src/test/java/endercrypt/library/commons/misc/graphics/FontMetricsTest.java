package endercrypt.library.commons.misc.graphics;


import endercrypt.library.commons.misc.Ender;

import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


public class FontMetricsTest
{
	private static final String sampleText = "Hello world, this is a test of FontMetrics !\"#¤%&/()=";
	
	private static FontMetricsTester tester;
	
	@BeforeAll
	static void beforeAll()
	{
		BufferedImage image = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);
		
		Graphics2D g2d = image.createGraphics();
		try
		{
			FontMetrics realFontMetrics = g2d.getFontMetrics();
			FontMetrics fakeFontMetrics = Ender.graphics.createFontMetrics(g2d.getFont());
			tester = new FontMetricsTester(realFontMetrics, fakeFontMetrics);
		}
		finally
		{
			g2d.dispose();
		}
	}
	
	@Test
	void testWidth()
	{
		tester.test("width", fm -> fm.stringWidth(sampleText));
	}
	
	@Test
	void testHeight()
	{
		tester.test("height", fm -> fm.getHeight());
	}
}
