package endercrypt.library.commons.misc;


import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import org.junit.jupiter.api.Test;


public class EnderExceptionTest
{
	@Test
	void toStringOf() throws InterruptedException
	{
		BlockingQueue<Exception> exceptionQueue = new LinkedBlockingDeque<>(1);
		
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				inner();
			}
			
			private void inner()
			{
				exceptionQueue.add(new Exception());
			}
		});
		
		try
		{
			thread.start();
			Exception exception = exceptionQueue.take();
			String message = Ender.exception.getStackTraceOf(exception);
			
			String expectedMessage = """
				java.lang.Exception
					at EnderCommons/endercrypt.library.commons.misc.EnderExceptionTest$1.inner(EnderExceptionTest.java:29)
					at EnderCommons/endercrypt.library.commons.misc.EnderExceptionTest$1.run(EnderExceptionTest.java:24)
					at java.base/java.lang.Thread.run(Thread.java:840)
				""";
			
			assertEquals(expectedMessage, message);
		}
		finally
		{
			thread.interrupt();
		}
	}
}
