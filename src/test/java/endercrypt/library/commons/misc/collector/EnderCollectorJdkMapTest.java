package endercrypt.library.commons.misc.collector;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.library.commons.entry.Entry;
import endercrypt.library.commons.misc.Ender;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;


public class EnderCollectorJdkMapTest
{
	@Test
	void collecting()
	{
		Map<String, Integer> original = new HashMap<>();
		original.put("foo", 5);
		original.put("bar", 10);
		
		Map<String, Integer> result = original.entrySet().stream()
			.map(e -> Entry.mapEntry(e.getKey(), e.getValue() * 10))
			.collect(Ender.collector.forJdkMapEntries());
		
		assertTrue(result.keySet().containsAll(original.keySet()));
		assertEquals(50, result.get("foo"));
		assertEquals(100, result.get("bar"));
	}
}
