package endercrypt.library.commons.misc.collector;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.library.commons.entry.Entry;
import endercrypt.library.commons.misc.Ender;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;


public class EnderCollectorMapTest
{
	@Test
	void collecting()
	{
		List<Entry<String, Integer>> original = new ArrayList<>();
		original.add(Entry.immutable("hello", 5));
		original.add(Entry.immutable("world", 10));
		
		Map<String, Integer> result = original.stream()
			.map(e -> Entry.immutable(e.getKey(), e.getValue() * 10))
			.collect(Ender.collector.forMapEntries());
		
		assertEquals(50, result.get("hello"));
		assertEquals(100, result.get("world"));
	}
}
