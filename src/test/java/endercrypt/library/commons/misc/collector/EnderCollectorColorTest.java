package endercrypt.library.commons.misc.collector;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.misc.collector.collectors.color.ColorCollectorException;
import endercrypt.library.commons.misc.color.ColorChannel;

import java.awt.Color;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;


public class EnderCollectorColorTest
{
	@Test
	void collectingSameColor()
	{
		Color originalColor = new Color(100, 50, 150);
		
		Color collectedColor = ColorChannel
			.streamValues(originalColor)
			.collect(Ender.collector.forColors());
		
		assertEquals(originalColor, collectedColor);
	}
	
	@Test
	void missingColorChannel()
	{
		Color originalColor = new Color(10, 5, 15);
		
		List<ColorChannel.Value> channels = ColorChannel.streamValues(originalColor).collect(Collectors.toList());
		channels.remove(0);
		
		assertThrows(ColorCollectorException.class, () -> {
			channels
				.stream()
				.collect(Ender.collector.forColors());
		});
	}
	
	@Test
	void defaultColor()
	{
		Color originalColor = new Color(101, 102, 103);
		Color defaultColor = new Color(0, 0, 0, 104);
		
		Color result = ColorChannel.streamValues(originalColor)
			.filter(c -> c.getChannel() != ColorChannel.ALPHA)
			.collect(Ender.collector.forColors(defaultColor));
		
		assertEquals(new Color(101, 102, 103, 104), result);
	}
}
