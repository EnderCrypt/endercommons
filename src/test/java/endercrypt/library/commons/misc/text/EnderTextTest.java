package endercrypt.library.commons.misc.text;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.library.commons.misc.Ender;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;


public class EnderTextTest
{
	@Test
	void formatWorks()
	{
		Map<String, String> variables = new HashMap<>();
		variables.put("noun", "world");
		
		String text = Ender.text.format("hello {{noun}}", variables);
		assertEquals("hello world", text);
	}
	
	@Test
	void formatErrorsOnMissingVariable()
	{
		Map<String, String> variables = new HashMap<>();
		
		assertThrowsExactly(IllegalArgumentException.class,
			() -> Ender.text.format("hello {{noun}}", variables));
	}
}
