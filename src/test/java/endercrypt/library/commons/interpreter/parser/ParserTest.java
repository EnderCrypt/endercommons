package endercrypt.library.commons.interpreter.parser;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.library.commons.interpreter.parser.steps.TokenMatcher;
import endercrypt.library.commons.interpreter.parser.steps.TokenReference;

import org.junit.jupiter.api.Test;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


public class ParserTest
{
	@Test
	void testWorks()
	{
		ParserFactory<Token> parserFactory = new ParserFactory<>();
		
		parserFactory.add(TokenMatcher.forClass(Token.Block.class));
		TokenReference<Token.Text> textReference = parserFactory.add(new TextTokenStartsWithMatcher("he"));
		parserFactory.add(TokenMatcher.forClass(Token.Block.class));
		
		Parser<Token> parser = parserFactory.create();
		parser.feed(new Token.Block());
		parser.feed(new Token.Text("hello world"));
		parser.feed(new Token.Block());
		
		assertEquals(ParserState.SUCCESS, parser.getState());
		assertEquals("hello world", parser.get(textReference).getValue());
	}
	
	@Test
	void testWrongToken()
	{
		ParserFactory<Token> parserFactory = new ParserFactory<>();
		
		parserFactory.add(TokenMatcher.forClass(Token.Block.class));
		parserFactory.add(new TextTokenStartsWithMatcher("he"));
		parserFactory.add(TokenMatcher.forClass(Token.Block.class));
		
		Parser<Token> parser = parserFactory.create();
		parser.feed(new Token.Block());
		parser.feed(new Token.Text("foo bar"));
		parser.feed(new Token.Block());
		
		assertEquals(ParserState.FAILURE, parser.getState());
	}
	
	@Test
	void testIncomplete()
	{
		ParserFactory<Token> parserFactory = new ParserFactory<>();
		
		parserFactory.add(TokenMatcher.forClass(Token.Block.class));
		parserFactory.add(new TextTokenStartsWithMatcher("he"));
		parserFactory.add(TokenMatcher.forClass(Token.Block.class));
		
		Parser<Token> parser = parserFactory.create();
		assertEquals(ParserState.PARTIAL, parser.getState());
		parser.feed(new Token.Block());
		assertEquals(ParserState.PARTIAL, parser.getState());
	}
	
	private static interface Token
	{
		@Getter
		@RequiredArgsConstructor
		public static class Text implements Token
		{
			private final String value;
		}
		
		public static class Block implements Token
		{
			
		}
	}
	
	@RequiredArgsConstructor
	private static class TextTokenStartsWithMatcher extends TokenMatcher.Abstract<Token.Text>
	{
		private final String text;
		
		@Override
		public boolean test(Token.Text token)
		{
			return token.getValue().toLowerCase().startsWith(text.toLowerCase());
		}
	}
}
