package endercrypt.library.commons.debug.sysout.hook;


import java.io.PrintStream;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Getter
public class PrintDestination
{
	public static PrintDestination getCurrent()
	{
		return new PrintDestination(System.out, System.err);
	}
	
	public static PrintDestination from(PrintStreamHook out, PrintStreamHook err)
	{
		return new PrintDestination(out.getPrintStream(), err.getPrintStream());
	}
	
	private final PrintStream out;
	private final PrintStream err;
	
	public void attach()
	{
		System.setOut(out);
		System.setErr(err);
	}
}
