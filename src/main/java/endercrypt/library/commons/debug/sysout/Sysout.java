package endercrypt.library.commons.debug.sysout;


import endercrypt.library.commons.debug.sysout.hook.PrintDestination;
import endercrypt.library.commons.debug.sysout.hook.PrintHook;


public class Sysout
{
	private static final BreakStrings strings = new BreakStrings();
	
	private static PrintHook hook;
	
	public static void throwOn(String text)
	{
		strings.add(text);
		if (hook == null)
		{
			PrintDestination destination = PrintDestination.getCurrent();
			hook = new PrintHook(destination, strings);
			hook.attach();
		}
	}
}
