package endercrypt.library.commons.debug.sysout.hook;


import endercrypt.library.commons.debug.sysout.BreakStrings;
import endercrypt.library.commons.debug.sysout.DebugPrintException;
import endercrypt.library.commons.debug.sysout.SysoutBuffer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class PrintStreamHook
{
	private final PrintStream destination;
	private final BreakStrings strings;
	
	@Getter
	private final PrintStream printStream = new PrintStream(new Forwarder());
	
	@Getter
	private final SysoutBuffer buffer = new SysoutBuffer();
	
	private class Forwarder extends OutputStream
	{
		@Override
		public void write(int b) throws IOException
		{
			destination.write(b);
			
			buffer.append(b);
			buffer.resize(strings.getMaxLength());
			strings.extractFrom(buffer).ifPresent(this::matchFound);
		}
		
		private void matchFound(String text)
		{
			strings.remove(text);
			
			throw new DebugPrintException(text);
		}
	}
}
