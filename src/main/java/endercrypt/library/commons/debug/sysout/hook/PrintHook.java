package endercrypt.library.commons.debug.sysout.hook;


import endercrypt.library.commons.debug.sysout.BreakStrings;

import lombok.Getter;


@Getter
public class PrintHook
{
	private final PrintDestination destination;
	private final PrintStreamHook out;
	private final PrintStreamHook err;
	private final PrintDestination hookStreams;
	
	public PrintHook(PrintDestination destination, BreakStrings strings)
	{
		this.destination = destination;
		out = new PrintStreamHook(destination.getOut(), strings);
		err = new PrintStreamHook(destination.getErr(), strings);
		hookStreams = PrintDestination.from(out, err);
	}
	
	public void attach()
	{
		hookStreams.attach();
	}
}
