package endercrypt.library.commons.debug.sysout;

@SuppressWarnings("serial")
public class DebugPrintException extends RuntimeException
{
	public DebugPrintException(String text)
	{
		super(text);
	}
}
