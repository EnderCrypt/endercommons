package endercrypt.library.commons.debug.sysout;

public class SysoutBuffer
{
	private final StringBuilder buffer = new StringBuilder();
	
	public void append(int b)
	{
		buffer.append((char) b);
	}
	
	public boolean resize(int size)
	{
		int oversized = size - buffer.length();
		if (oversized > 0)
		{
			buffer.delete(0, oversized);
			buffer.setLength(size);
			return true;
		}
		return false;
	}
	
	public boolean matches(String text)
	{
		int end = buffer.length();
		int start = end - text.length();
		String other = buffer.substring(start, end);
		return text.equals(other);
	}
}
