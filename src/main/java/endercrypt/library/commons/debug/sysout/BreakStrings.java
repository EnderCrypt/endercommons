package endercrypt.library.commons.debug.sysout;


import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import lombok.Getter;


public class BreakStrings
{
	private final Set<String> strings = new HashSet<>();
	
	@Getter
	private int maxLength = 0;
	
	public void add(String text)
	{
		strings.add(text);
		calculateMaxLength();
	}
	
	public void remove(String text)
	{
		strings.remove(text);
		calculateMaxLength();
	}
	
	private void calculateMaxLength()
	{
		maxLength = strings.stream().mapToInt(String::length).max().orElse(0);
	}
	
	public Optional<String> extractFrom(SysoutBuffer buffer)
	{
		return strings.stream()
			.filter(buffer::matches)
			.findFirst();
	}
}
