package endercrypt.library.commons.interpreter.parser;


import java.lang.reflect.ParameterizedType;


public class TypeReference<T>
{
	public TypeReference()
	{
		if (getClass().equals(TypeReference.class))
		{
			throw new IllegalArgumentException("subclass by using: new TypeReference<>() {{}}");
		}
	}
	
	public Class<T> getReferencedClass()
	{
		System.out.println(getClass().getGenericSuperclass().getClass());
		ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
		System.out.println(parameterizedType.getActualTypeArguments()[0]);
		return null;
	}
}
