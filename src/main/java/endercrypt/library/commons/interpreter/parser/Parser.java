package endercrypt.library.commons.interpreter.parser;


import endercrypt.library.commons.interpreter.parser.steps.TokenMatcher;
import endercrypt.library.commons.interpreter.parser.steps.TokenParserStep;
import endercrypt.library.commons.interpreter.parser.steps.TokenReference;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.NonNull;


public class Parser<T>
{
	private final List<TokenParserStep<? extends T>> steps;
	private final Map<TokenReference<? extends T>, T> result = new HashMap<>();
	private int index = 0;
	
	@Getter
	private ParserState state = ParserState.PARTIAL;
	
	protected Parser(List<TokenParserStep<? extends T>> matchers)
	{
		this.steps = List.copyOf(matchers);
	}
	
	public boolean isFinished()
	{
		return state != ParserState.PARTIAL;
	}
	
	public boolean isAtEnd()
	{
		return index >= steps.size();
	}
	
	@SuppressWarnings("unchecked")
	public <K extends T> void feed(T token)
	{
		if (isAtEnd())
		{
			return;
		}
		if (isFinished())
		{
			return;
		}
		TokenParserStep<K> step = (TokenParserStep<K>) steps.get(index);
		if (step.getTokenClass().isAssignableFrom(token.getClass()) == false)
		{
			state = ParserState.FAILURE;
			return;
		}
		TokenMatcher<K> matcher = step.getMatcher();
		boolean success = matcher.test((K) token);
		
		if (success == false)
		{
			state = ParserState.FAILURE;
			return;
		}
		
		TokenReference<K> tokenReference = step.getReference();
		result.put(tokenReference, token);
		index++;
		if (isAtEnd())
		{
			state = ParserState.SUCCESS;
		}
	}
	
	@SuppressWarnings("unchecked")
	public <K extends T> K get(@NonNull TokenReference<K> tokenReference)
	{
		if (getState() != ParserState.SUCCESS)
		{
			throw new IllegalStateException("Can only obtain parser results from a succesful parser");
		}
		K value = (K) result.get(tokenReference);
		if (value == null)
		{
			throw new IllegalStateException(TokenReference.class.getSimpleName() + " has no appropriate results");
		}
		return value;
	}
}
