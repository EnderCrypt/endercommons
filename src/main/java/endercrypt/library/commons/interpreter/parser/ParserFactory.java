package endercrypt.library.commons.interpreter.parser;


import endercrypt.library.commons.interpreter.parser.steps.TokenMatcher;
import endercrypt.library.commons.interpreter.parser.steps.TokenParserStep;
import endercrypt.library.commons.interpreter.parser.steps.TokenReference;

import java.util.ArrayList;
import java.util.List;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class ParserFactory<T>
{
	protected final List<TokenParserStep<? extends T>> steps = new ArrayList<>();
	
	public <K extends T> TokenReference<K> add(TokenMatcher<K> matcher)
	{
		TokenReference<K> reference = new TokenReference<>();
		TokenParserStep<K> step = new TokenParserStep<K>(matcher, reference);
		steps.add(step);
		return reference;
	}
	
	public Parser<T> create()
	{
		return new Parser<>(steps);
	}
}
