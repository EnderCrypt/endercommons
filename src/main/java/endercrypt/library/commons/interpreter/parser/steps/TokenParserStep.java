package endercrypt.library.commons.interpreter.parser.steps;


import lombok.Getter;


@Getter
public class TokenParserStep<T>
{
	private final TokenMatcher<T> matcher;
	
	private final TokenReference<T> reference;
	
	private final Class<T> tokenClass;
	
	public TokenParserStep(TokenMatcher<T> matcher, TokenReference<T> reference)
	{
		this.matcher = matcher;
		this.reference = reference;
		this.tokenClass = matcher.getTargetClass();
	}
}
