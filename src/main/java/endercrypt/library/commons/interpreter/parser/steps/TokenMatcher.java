package endercrypt.library.commons.interpreter.parser.steps;


import java.lang.reflect.ParameterizedType;

import lombok.Getter;


public interface TokenMatcher<T>
{
	public static <T> TokenMatcher<T> forClass(Class<T> targetClass)
	{
		return new TokenMatcher<T>()
		{
			@Override
			public Class<T> getTargetClass()
			{
				return targetClass;
			}
			
			@Override
			public boolean test(T token)
			{
				return targetClass.isAssignableFrom(token.getClass());
			}
		};
	}
	
	@Getter
	public static abstract class Abstract<T> implements TokenMatcher<T>
	{
		private final Class<T> targetClass;
		
		@SuppressWarnings("unchecked")
		public Abstract()
		{
			ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
			targetClass = (Class<T>) parameterizedType.getActualTypeArguments()[0];
		}
	}
	
	/**
	 * sorry, not reifiable generic ... (use TokenMatcher.Abstract to
	 * automatically implement)
	 */
	public Class<T> getTargetClass();
	
	public boolean test(T token);
}
