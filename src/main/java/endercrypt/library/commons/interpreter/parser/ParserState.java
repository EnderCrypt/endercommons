package endercrypt.library.commons.interpreter.parser;

public enum ParserState
{
	PARTIAL,
	FAILURE,
	SUCCESS;
}
