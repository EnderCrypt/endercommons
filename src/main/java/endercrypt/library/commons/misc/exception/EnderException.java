package endercrypt.library.commons.misc.exception;


import endercrypt.library.commons.misc.Ender;

import java.io.PrintWriter;
import java.io.StringWriter;


public class EnderException extends Ender
{
	public String getStackTraceOf(Throwable e)
	{
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		return stringWriter.toString();
	}
	
	@SuppressWarnings("unchecked")
	public <E extends Throwable> void sneakyThrow(Throwable e) throws E
	{
		throw (E) e;
	}
}
