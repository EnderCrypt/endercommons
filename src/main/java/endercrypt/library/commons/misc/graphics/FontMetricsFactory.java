package endercrypt.library.commons.misc.graphics;


import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import lombok.Getter;


public class FontMetricsFactory
{
	private final BufferedImage dummyImage;
	private final Graphics2D dummyGraphics;
	
	@Getter
	private final FontMetrics defaultFontMetrics;
	
	public FontMetricsFactory()
	{
		dummyImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		dummyGraphics = dummyImage.createGraphics();
		defaultFontMetrics = dummyGraphics.getFontMetrics();
	}
	
	public FontMetrics create(Font font)
	{
		return dummyGraphics.getFontMetrics(font);
	}
	
	public void dispose()
	{
		dummyGraphics.dispose();
		dummyImage.flush();
	}
}
