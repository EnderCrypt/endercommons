package endercrypt.library.commons.misc.graphics;


import endercrypt.library.commons.misc.Ender;

import java.awt.Font;
import java.awt.FontMetrics;


public class EnderGraphics extends Ender
{
	private final FontMetricsFactory fontMetricsFactory = new FontMetricsFactory();
	
	public FontMetrics getDefaultFontMetrics()
	{
		return fontMetricsFactory.getDefaultFontMetrics();
	}
	
	public FontMetrics createFontMetrics(Font font)
	{
		return fontMetricsFactory.create(font);
	}
}
