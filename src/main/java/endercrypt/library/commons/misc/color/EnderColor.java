package endercrypt.library.commons.misc.color;


import endercrypt.library.commons.misc.Ender;

import java.awt.Color;


public class EnderColor extends Ender
{
	public Color invert(Color color, boolean includingAlpha)
	{
		return transform(color, (channel, value) -> 255 - value, includingAlpha);
	}
	
	public int getColorDifference(Color color1, Color color2, boolean includeAlpha)
	{
		return ColorChannel.streamChannels()
			.filter(channel -> includeAlpha || channel != ColorChannel.ALPHA)
			.mapToInt(channel -> channel.extract(color1) - channel.extract(color2))
			.map(Math::abs)
			.sum();
	}
	
	public Color transform(Color color, ColorValueTransformer transformer, boolean includeAlpha)
	{
		return ColorChannel.streamChannels()
			.filter(channel -> includeAlpha || channel != ColorChannel.ALPHA)
			.map(channel -> channel.create(transformer.transform(channel, channel.extract(color))))
			.collect(Ender.collector.forColors(color));
	}
}
