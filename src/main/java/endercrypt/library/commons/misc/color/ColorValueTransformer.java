package endercrypt.library.commons.misc.color;

public interface ColorValueTransformer
{
	public int transform(ColorChannel channel, int value);
}