package endercrypt.library.commons.misc.color;


import java.awt.Color;
import java.util.Arrays;
import java.util.stream.Stream;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public enum ColorChannel
{
	ALPHA(3, Color::getAlpha),
	RED(2, Color::getRed),
	GREEN(1, Color::getGreen),
	BLUE(0, Color::getBlue);
	
	@Getter
	private final int bytePosition;
	private final ColorGetter colorGetter;
	
	public int getBitPosition()
	{
		return bytePosition * 8;
	}
	
	public int extract(Color color)
	{
		return colorGetter.get(color);
	}
	
	public ColorChannel.Value extractAsValue(Color color)
	{
		return create(extract(color));
	}
	
	public int extract(int color)
	{
		return (color >> getBitPosition()) & 0xff;
	}
	
	public ColorChannel.Value extractAsValue(int color)
	{
		return create(extract(color));
	}
	
	public ColorChannel.Value create(int value)
	{
		return new ColorChannel.Value(value);
	}
	
	public static Stream<ColorChannel> streamChannels()
	{
		return Arrays.stream(values());
	}
	
	public static Stream<ColorChannel.Value> streamValues(Color color)
	{
		return streamChannels()
			.map(channel -> channel.extractAsValue(color));
	}
	
	private interface ColorGetter
	{
		public int get(Color color);
	}
	
	@EqualsAndHashCode(onlyExplicitlyIncluded = true)
	public final class Value
	{
		@EqualsAndHashCode.Include
		private final int value;
		
		public Value(int value)
		{
			if (value < 0)
			{
				throw new IllegalArgumentException("value for "
					+ ColorChannel.class.getSimpleName() + "." + ColorChannel.this.toString()
					+ " is " + value
					+ ", but cant be below 0");
			}
			if (value > 255)
			{
				throw new IllegalArgumentException("value for "
					+ ColorChannel.class.getSimpleName() + "." + ColorChannel.this.toString()
					+ " is " + value
					+ ", but cant be above 255");
			}
			this.value = value;
		}
		
		public ColorChannel getChannel()
		{
			return ColorChannel.this;
		}
		
		public int asInteger()
		{
			return value;
		}
		
		public float asFloat()
		{
			return 1.0f / 255 * asInteger();
		}
	}
}
