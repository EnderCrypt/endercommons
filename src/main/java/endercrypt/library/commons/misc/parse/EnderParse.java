package endercrypt.library.commons.misc.parse;


import endercrypt.library.commons.interfaces.EqualsCheck;
import endercrypt.library.commons.misc.Ender;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;


public class EnderParse extends Ender
{
	public Optional<Boolean> toBoolean(String value)
	{
		if (value == null)
		{
			return Optional.empty();
		}
		if (value.equalsIgnoreCase("true"))
		{
			return Optional.of(true);
		}
		if (value.equalsIgnoreCase("false"))
		{
			return Optional.of(false);
		}
		return Optional.empty();
	}
	
	// NUMBER
	
	public Optional<Byte> toByte(String value)
	{
		return toNumber(Byte::parseByte, value);
	}
	
	public Optional<Short> toShort(String value)
	{
		return toNumber(Short::parseShort, value);
	}
	
	public Optional<Integer> toInteger(String value)
	{
		return toNumber(Integer::parseInt, value);
	}
	
	public Optional<Long> toLong(String value)
	{
		return toNumber(Long::parseLong, value);
	}
	
	public Optional<Float> toFloat(String value)
	{
		return toNumber(Float::parseFloat, value);
	}
	
	public Optional<Double> toDouble(String value)
	{
		return toNumber(Double::parseDouble, value);
	}
	
	public Optional<BigDecimal> toBigDecimal(String value)
	{
		return toNumber(BigDecimal::new, value);
	}
	
	public Optional<BigInteger> toBigInteger(String value)
	{
		return toNumber(BigInteger::new, value);
	}
	
	private <T extends Number> Optional<T> toNumber(Function<String, T> parser, String value)
	{
		return parse(parser::apply, value, NumberFormatException.class);
	}
	
	// URL
	
	public Optional<URI> toUri(String value)
	{
		return parse(URI::new, value, URISyntaxException.class);
	}
	
	public Optional<URL> toUrl(String value)
	{
		return parse(URL::new, value, MalformedURLException.class);
	}
	
	public Optional<URL> toUrl(URL url, String spec)
	{
		return parse(() -> new URL(url, spec), MalformedURLException.class);
	}
	
	// UUID
	
	public Optional<UUID> toUUID(String value)
	{
		return parse(UUID::fromString, value, IllegalArgumentException.class);
	}
	
	// ENUM
	
	public <T extends Enum<T>> Optional<T> toEnum(Class<T> enumClass, String value)
	{
		return toEnum(enumClass, true, value);
	}
	
	public <T extends Enum<T>> Optional<T> toEnum(Class<T> enumClass, boolean caseSensitive, String value)
	{
		EqualsCheck<String> equalsCheck = EqualsCheck.forString(caseSensitive);
		return toEnum(enumClass, equalsCheck, value);
	}
	
	private <T extends Enum<T>> Optional<T> toEnum(Class<T> enumClass, EqualsCheck<String> stringEquals, String value)
	{
		for (T enumInstance : enumClass.getEnumConstants())
		{
			if (stringEquals.isEquals(enumInstance.name(), value))
			{
				return Optional.of(enumInstance);
			}
		}
		return Optional.empty();
	}
	
	// CLASS
	
	public Optional<Class<?>> toClass(String className)
	{
		return parse(Class::forName, className, ClassNotFoundException.class);
	}
	
	public Optional<Method> toMethod(Class<?> clazz, String methodName, Class<?>... args)
	{
		return parse(() -> clazz.getMethod(methodName, args), NoSuchMethodException.class);
	}
	
	// ABSTRACTION
	
	private <T, E extends Exception> Optional<T> parse(MethodReferenceParser<T, E> parser, String value, Class<E> exceptionClass)
	{
		return parse(() -> parser.parse(value), exceptionClass);
	}
	
	private <T, E extends Exception> Optional<T> parse(EmptyParser<T, E> parser, Class<E> exceptionClass)
	{
		try
		{
			return Optional.of(parser.parse());
		}
		catch (Exception e)
		{
			if (exceptionClass.isInstance(e))
			{
				return Optional.empty();
			}
			throw new EnderParseException("expected exception " + exceptionClass.getName() + " from parsing, but got " + e.getClass().getName(), e);
		}
	}
	
	public interface EmptyParser<T, E extends Exception>
	{
		public T parse() throws E;
	}
	
	public interface MethodReferenceParser<T, E extends Exception>
	{
		public T parse(String text) throws E;
	}
}
