package endercrypt.library.commons.misc.parse;

public class EnderParseException extends RuntimeException
{
	private static final long serialVersionUID = 6914435580636651174L;
	
	public EnderParseException(String message)
	{
		super(message);
	}
	
	public EnderParseException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
