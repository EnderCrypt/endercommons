package endercrypt.library.commons.misc.format;


import endercrypt.library.commons.misc.Ender;


public class EnderFormat extends Ender
{
	public String thousandMarks(double value)
	{
		String text = String.valueOf(value);
		int decimalSeparatorIndex = findDecimalSeparator(text);
		String integerPart = text.substring(0, decimalSeparatorIndex);
		String decimalPart = text.substring(decimalSeparatorIndex);
		return thousandMarks(integerPart) + decimalPart;
	}
	
	private int findDecimalSeparator(String text)
	{
		int dotIndex = text.lastIndexOf(".");
		if (dotIndex != -1)
		{
			return dotIndex;
		}
		
		int commaIndex = text.lastIndexOf(",");
		if (commaIndex != -1)
		{
			return commaIndex;
		}
		
		throw new IllegalArgumentException("No (known) decimal separator found in text: " + text);
	}
	
	public String thousandMarks(long value)
	{
		return thousandMarks(String.valueOf(value));
	}
	
	private String thousandMarks(String number)
	{
		StringBuilder builder = new StringBuilder(number);
		int index = builder.length();
		int thousandCounter = 3;
		while (index > 0)
		{
			if (thousandCounter == 0 && builder.charAt(index - 1) != '-')
			{
				builder.insert(index, "'");
				index++;
				thousandCounter = 4;
			}
			index--;
			thousandCounter--;
		}
		return builder.toString();
	}
}
