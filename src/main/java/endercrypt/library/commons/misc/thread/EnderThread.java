package endercrypt.library.commons.misc.thread;


import endercrypt.library.commons.misc.Ender;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;


public class EnderThread extends Ender
{
	private final ExecutorService executor = Executors.newCachedThreadPool();
	
	public void sneakySleep(int quantity, TimeUnit unit)
	{
		try
		{
			sleep(quantity, unit);
		}
		catch (InterruptedException e)
		{
			Ender.exception.sneakyThrow(e);
		}
	}
	
	public void sleep(int quantity, TimeUnit unit) throws InterruptedException
	{
		long time = unit.toMillis(quantity);
		Thread.sleep(time);
	}
	
	/**
	 * some blocking methods dont throw {@link InterruptedException} most
	 * notably {@link java.net.ServerSocket#accept()} the only way to gracefully
	 * close it is to let another thread wait untill interrupt, then call
	 * {@link java.net.ServerSocket#close()}
	 */
	public void waitUntillInterrupt()
	{
		Object object = new Object();
		try
		{
			while (true)
			{
				synchronized (object)
				{
					object.wait();
				}
			}
		}
		catch (InterruptedException e)
		{
			return;
		}
	}
	
	/**
	 * primarily designed to let you consume stdin/stderr of a process to
	 * prevent a deadlock
	 */
	public void consumeWholeInputStreamInBackground(InputStream stream)
	{
		executor.execute(new SilentNullInputStreamConsumer(stream));
	}
	
	@RequiredArgsConstructor
	private static class SilentNullInputStreamConsumer implements Runnable
	{
		private final InputStream stream;
		private int errorCount = 0;
		
		@Override
		public void run()
		{
			while (true)
			{
				try
				{
					if (stream.read() == -1)
						return; // done
				}
				catch (IOException e)
				{
					errorCount++;
					if (errorCount > 1000)
						return; // too many errors
				}
			}
		}
	}
}
