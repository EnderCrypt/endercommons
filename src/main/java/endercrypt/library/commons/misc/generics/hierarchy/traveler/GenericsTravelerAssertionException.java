package endercrypt.library.commons.misc.generics.hierarchy.traveler;


import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;


/**
 * If this class gets thrown from {@link GenericsTraveler} then dont worry, you
 * didnt do anything wrong. generics is complicated and this is essentially an
 * {@link AssertionError} on my part.
 * 
 * @author endercrypt
 */
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class GenericsTravelerAssertionException extends RuntimeException
{
	private static final long serialVersionUID = -7903099783243437575L;
	
	public GenericsTravelerAssertionException(String message)
	{
		super(message);
	}
	
	public GenericsTravelerAssertionException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
