package endercrypt.library.commons.misc.generics;


import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.misc.generics.hierarchy.GenericClassLayer;
import endercrypt.library.commons.misc.generics.hierarchy.GenericDefinition;
import endercrypt.library.commons.misc.generics.hierarchy.GenericDefinition.Resolved;
import endercrypt.library.commons.misc.generics.hierarchy.GenericDefinition.Unresolved;
import endercrypt.library.commons.misc.generics.hierarchy.GenericsClassHierarchy;
import endercrypt.library.commons.misc.generics.hierarchy.traveler.GenericsTraveler;
import endercrypt.library.commons.misc.generics.hierarchy.traveler.GenericsTravelerAssertionException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class EnderGenerics extends Ender
{
	/**
	 * Usually should be called from the parent class with getClass() as
	 * parameter
	 * 
	 * @param targetClass
	 *     the class which define a generic class
	 * @return an array of all defined generic classes
	 */
	public Class<?>[] getDefinitions(Class<?> targetClass)
	{
		Type genericSuperClass = targetClass.getGenericSuperclass();
		if (genericSuperClass instanceof ParameterizedType == false)
		{
			throw new IllegalArgumentException(targetClass + " does not define any generic types");
		}
		ParameterizedType type = (ParameterizedType) genericSuperClass;
		Type[] types = type.getActualTypeArguments();
		
		return Arrays.stream(types)
			.filter(Class.class::isInstance)
			.map(Class.class::cast)
			.toArray(Class[]::new);
	}
	
	private final Map<Class<?>, GenericsClassHierarchy> genericsHierarchy = new HashMap<>();
	
	public synchronized GenericsClassHierarchy getGenericsClassHierarchy(Class<?> targetClass)
	{
		GenericsClassHierarchy hierarchy = genericsHierarchy.get(targetClass);
		if (hierarchy == null)
		{
			hierarchy = GenericsTraveler.resolve(targetClass);
		}
		return hierarchy;
	}
	
	/**
	 * Attempts to look through the whole class hierarchy to automatically
	 * resolve the class of a generic variable
	 * 
	 * expects the generic which needs to be looked up to be in the caller class
	 * 
	 * @param trueClass
	 *     the root class, usually obtained via getClass()
	 * @param name
	 *     the name of the generic
	 * @return the reified class type
	 */
	public Class<?> getGenericClass(Class<?> trueClass, String name)
	{
		return getGenericClass(trueClass, name, Ender.clazz.getCallerClass());
	}
	
	/**
	 * Attempts to look through the whole class hierarchy to automatically
	 * resolve the class of a generic variable
	 * 
	 * @param trueClass
	 *     the root class, usually obtained via getClass()
	 * @param name
	 *     the name of the generic
	 * @param localClass
	 *     the class which requires the generic, usually the caller class
	 * @return the reified class type
	 */
	public Class<?> getGenericClass(Class<?> trueClass, String name, Class<?> localClass)
	{
		GenericsClassHierarchy genericsClassHierarchy = getGenericsClassHierarchy(trueClass);
		GenericClassLayer layer = genericsClassHierarchy.getLayer(localClass).orElseThrow(() -> new GenericsTravelerAssertionException("The generics hierarchy generated for " + trueClass + " did not contain a layer for that same class"));
		GenericDefinition definition = layer.getDefinition(name).orElseThrow(() -> new IllegalArgumentException("No generic with name " + name + " exists in " + localClass));
		if (definition instanceof Unresolved)
		{
			throw new IllegalArgumentException(trueClass + " and its super classes did not define the generic " + name + ", located in " + localClass);
		}
		Resolved resolved = (Resolved) definition;
		return resolved.getDeclaredClass();
	}
}
