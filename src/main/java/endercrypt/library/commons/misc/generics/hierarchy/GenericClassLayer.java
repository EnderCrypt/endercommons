package endercrypt.library.commons.misc.generics.hierarchy;


import endercrypt.library.commons.misc.generics.hierarchy.GenericDefinition.Unresolved;
import endercrypt.library.commons.pubimm.PublicallyImmutableList;

import java.util.Collection;
import java.util.Optional;

import lombok.Getter;


@Getter
@SuppressWarnings("serial")
public class GenericClassLayer extends PublicallyImmutableList<GenericDefinition>
{
	private final Class<?> targetClass;
	
	public GenericClassLayer(Class<?> targetClass, Collection<GenericDefinition> definitions)
	{
		this.targetClass = targetClass;
		elements.addAll(definitions);
	}
	
	public boolean isPartial()
	{
		for (GenericDefinition definition : elements)
		{
			if (definition instanceof Unresolved)
			{
				return false;
			}
		}
		return true;
	}
	
	public Optional<GenericDefinition> getDefinition(String name)
	{
		return stream()
			.filter(definition -> definition.getName().equals(name))
			.findFirst();
	}
	
	@Override
	public String toString()
	{
		return getTargetClass() + " -> " + super.toString();
	}
}
