package endercrypt.library.commons.misc.generics.hierarchy;


import endercrypt.library.commons.misc.generics.hierarchy.GenericDefinition.Resolved;
import endercrypt.library.commons.misc.generics.hierarchy.GenericDefinition.Unresolved;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Getter
public sealed abstract class GenericDefinition permits Resolved, Unresolved
{
	private final String name;
	
	@Override
	public String toString()
	{
		String genericClassString = "?";
		if (this instanceof Resolved resolved)
		{
			genericClassString = resolved.getDeclaredClass().getSimpleName();
		}
		return getName() + "=" + genericClassString;
	}
	
	@Getter
	public static final class Resolved extends GenericDefinition
	{
		private final Class<?> definingClass;
		private final Class<?> declaredClass;
		
		public Resolved(String name, Class<?> definingClass, Class<?> declaredClass)
		{
			super(name);
			this.definingClass = definingClass;
			this.declaredClass = declaredClass;
		}
	}
	
	@Getter
	public static final class Unresolved extends GenericDefinition
	{
		public Unresolved(String name)
		{
			super(name);
		}
	}
}
