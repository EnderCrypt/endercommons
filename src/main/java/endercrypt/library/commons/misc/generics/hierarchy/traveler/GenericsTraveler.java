package endercrypt.library.commons.misc.generics.hierarchy.traveler;


import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.misc.generics.hierarchy.GenericClassLayer;
import endercrypt.library.commons.misc.generics.hierarchy.GenericDefinition;
import endercrypt.library.commons.misc.generics.hierarchy.GenericsClassHierarchy;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class GenericsTraveler
{
	public static GenericsClassHierarchy resolve(Class<?> targetClass)
	{
		GenericsTraveler traveler = new GenericsTraveler();
		
		List<GenericClassLayer> layers = Ender.clazz.getClassHierarchy(targetClass)
			.stream()
			.map(traveler::travel)
			.toList();
		
		return new GenericsClassHierarchy(targetClass, layers);
	}
	
	private GenericsMemory previousMemory = new GenericsMemory();
	
	public GenericClassLayer travel(Class<?> targetClass)
	{
		// System.out.println("--> " + targetClass.getSimpleName());
		// System.out.println("memory:      " + previousMemory);
		Class<?> parentClass = targetClass.getSuperclass();
		
		List<GenericDefinition> targetClassDefinitions = new ArrayList<>();
		for (TypeVariable<?> parameter : targetClass.getTypeParameters())
		{
			String name = parameter.getName();
			GenericDefinition genericClass = previousMemory.read(name);
			targetClassDefinitions.add(genericClass);
			// System.out.println("READ " + genericClass);
		}
		
		// next memory
		GenericsMemory nextMemory = new GenericsMemory();
		
		if (targetClass.getGenericSuperclass() instanceof ParameterizedType parameterized)
		{
			Type[] typeDefinitions = parameterized.getActualTypeArguments();
			// System.out.println("definitions: " + Arrays.toString(typeDefinitions)); // definitions i pass upwards
			TypeVariable<?>[] parentGenerics = parentClass.getTypeParameters();
			// System.out.println("parent:      " + Arrays.toString(parentGenerics)); // generics my parent expects from me
			
			if (typeDefinitions.length != parentGenerics.length)
			{
				throw new GenericsTravelerAssertionException(targetClass + " defines the generics " + Arrays.toString(typeDefinitions) + " for its parent( " + parentClass + ") however this does not match its expectectation of: " + Arrays.toString(parentGenerics));
			}
			
			// save generics for the parent
			for (int i = 0; i < typeDefinitions.length; i++)
			{
				Type type = typeDefinitions[i];
				TypeVariable<?> parentGeneric = parentGenerics[i];
				String name = parentGeneric.getName();
				GenericDefinition genericClsas = resolveClassForType(name, targetClass, type);
				// System.out.println("WRITE " + genericClsas);
				nextMemory.write(genericClsas);
			}
		}
		
		previousMemory = nextMemory;
		
		return new GenericClassLayer(targetClass, targetClassDefinitions);
	}
	
	private GenericDefinition resolveClassForType(String genericName, Class<?> targetClass, Type type)
	{
		if (type instanceof Class<?> concreteClass)
		{
			return new GenericDefinition.Resolved(genericName, targetClass, concreteClass);
		}
		if (type instanceof TypeVariable<?> typeVariable)
		{
			String name = typeVariable.getName();
			return previousMemory.read(name);
		}
		throw new GenericsTravelerAssertionException("the getActualTypeArguments() of a ParameterizedType was of type " + type.getClass() + " which is not one that is expected");
	}
}
