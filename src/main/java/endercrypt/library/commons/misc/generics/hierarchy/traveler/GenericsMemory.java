package endercrypt.library.commons.misc.generics.hierarchy.traveler;


import endercrypt.library.commons.misc.generics.hierarchy.GenericDefinition;

import java.util.HashMap;
import java.util.Map;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class GenericsMemory
{
	private final Map<String, GenericDefinition> definitions = new HashMap<>();
	
	public void write(@NonNull GenericDefinition genericClass)
	{
		definitions.put(genericClass.getName(), genericClass);
	}
	
	public GenericDefinition read(@NonNull String genericName)
	{
		GenericDefinition genericClass = definitions.get(genericName);
		if (genericClass == null)
		{
			genericClass = new GenericDefinition.Unresolved(genericName);
		}
		return genericClass;
	}
	
	@Override
	public String toString()
	{
		return definitions.values().toString();
	}
}
