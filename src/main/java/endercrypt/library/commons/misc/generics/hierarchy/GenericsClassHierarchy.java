package endercrypt.library.commons.misc.generics.hierarchy;


import endercrypt.library.commons.pubimm.PublicallyImmutableList;

import java.util.Collection;
import java.util.Optional;

import lombok.Getter;


@SuppressWarnings("serial")
@Getter
public class GenericsClassHierarchy extends PublicallyImmutableList<GenericClassLayer>
{
	private final Class<?> targetClass;
	
	public GenericsClassHierarchy(Class<?> targetClass, Collection<GenericClassLayer> layers)
	{
		this.targetClass = targetClass;
		elements.addAll(layers);
	}
	
	public boolean isPartial()
	{
		return elements.stream()
			.anyMatch(GenericClassLayer::isPartial);
	}
	
	public Optional<GenericClassLayer> getLayer(Class<?> targetClass)
	{
		return elements
			.stream()
			.filter(layer -> layer.getTargetClass().equals(targetClass))
			.findFirst();
	}
}
