package endercrypt.library.commons.misc.stream;


import endercrypt.library.commons.misc.Ender;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


public class EnderStream extends Ender
{
	public <E> Stream<E> from(Enumeration<E> enumeration)
	{
		return from(enumeration.asIterator());
	}
	
	public <E> Stream<E> from(Iterable<E> iterable)
	{
		return from(iterable.spliterator());
	}
	
	public <E> Stream<E> from(Iterator<E> iterator)
	{
		return from(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED));
	}
	
	public <E> Stream<E> from(Spliterator<E> spliterator)
	{
		return StreamSupport.stream(spliterator, false);
	}
	
}
