package endercrypt.library.commons.misc.text;


import endercrypt.library.commons.misc.Ender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


public class EnderText extends Ender
{
	public String capitalize(String text)
	{
		if (text == null || text.isBlank())
		{
			return text;
		}
		return Character.toUpperCase(text.charAt(0)) + text.substring(1);
	}
	
	public String repeat(char c, int count)
	{
		return String.valueOf(c).repeat(count);
	}
	
	public String multilineStrip(String text)
	{
		text = text.strip();
		
		text = Arrays.stream(text.split("\n"))
			.map(String::strip)
			.collect(Collectors.joining("\n"));
		
		return text;
	}
	
	public String friendlyJoin(Object[] values)
	{
		return friendlyJoin(Arrays.stream(values));
	}
	
	public String friendlyJoin(Collection<? extends Object> values)
	{
		return friendlyJoin(values.stream());
	}
	
	public String friendlyJoin(Stream<? extends Object> values)
	{
		return friendlyJoin(values.map(String::valueOf).collect(Collectors.toList()));
	}
	
	public String friendlyJoin(List<? extends Object> values)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < values.size(); i++)
		{
			String text = String.valueOf(values.get(i));
			sb.append(text);
			if (i < values.size() - 1)
			{
				String separator = (i == values.size() - 2) ? " and " : ", ";
				sb.append(separator);
			}
		}
		return sb.toString();
	}
	
	public double getNormalizedLevenshteinDistance(String left, String right)
	{
		int distance = getLevenshteinDistance(left, right);
		int stringLength = Math.max(left.length(), right.length());
		if (stringLength == 0)
		{
			return 0.0;
		}
		return 1.0 / stringLength * distance;
	}
	
	/*
	 * Cleaned up version from
	 * 
	 * Apache "Commons Text" 1.10.0
	 */
	public int getLevenshteinDistance(String left, String right)
	{
		int leftLength = left.length();
		int rightLength = right.length();
		
		if (leftLength == 0)
			return rightLength;
		
		if (rightLength == 0)
			return leftLength;
		
		if (leftLength > rightLength)
		{
			String oldRight = right;
			right = left;
			left = oldRight;
			rightLength = leftLength;
			leftLength = left.length();
		}
		
		int[] array = new int[leftLength + 1];
		
		int indexLeft;
		for (indexLeft = 0; indexLeft <= leftLength; indexLeft++)
		{
			array[indexLeft] = indexLeft;
		}
		
		for (int indexRight = 1; indexRight <= rightLength; indexRight++)
		{
			int upperLeft = array[0];
			int rightCharacter = right.charAt(indexRight - 1);
			array[0] = indexRight;
			
			for (indexLeft = 1; indexLeft <= leftLength; indexLeft++)
			{
				int upper = array[indexLeft];
				int cost = left.charAt(indexLeft - 1) == rightCharacter ? 0 : 1;
				array[indexLeft] = Ender.math.min(array[indexLeft - 1] + 1, array[indexLeft] + 1, upperLeft + cost);
				upperLeft = upper;
			}
		}
		
		return array[leftLength];
	}
	
	public String padLeft(String text, char padding, int length)
	{
		return pad(text, padding, length, (join) -> join + text);
	}
	
	public String padRight(String text, char padding, int length)
	{
		return pad(text, padding, length, (join) -> text + join);
	}
	
	private String pad(String text, char padding, int length, UnaryOperator<String> padder)
	{
		int required = length - text.length();
		if (required <= 0)
		{
			return text;
		}
		return padder.apply(repeat(padding, required));
	}
	
	public Optional<String> removeBeginning(String text, String match)
	{
		return removeByIndex(text, match, 0);
	}
	
	public Optional<String> removeEnding(String text, String match)
	{
		return removeByIndex(text, match, text.length() - match.length());
	}
	
	public Optional<String> removeMiddle(String text, String match)
	{
		return removeByIndex(text, match, text.indexOf(match));
	}
	
	private Optional<String> removeByIndex(String text, String match, int indexStart)
	{
		if (indexStart == -1)
		{
			return Optional.empty();
		}
		int indexEnd = indexStart + match.length();
		if (indexEnd > text.length())
		{
			return Optional.empty();
		}
		String section = text.substring(indexStart, indexEnd);
		if (section.equals(match) == false)
		{
			return Optional.empty();
		}
		String start = text.substring(0, indexStart);
		String end = text.substring(indexEnd, text.length());
		return Optional.of(start + end);
	}
	
	public String tab(String text)
	{
		return tab(text, 1);
	}
	
	public String tab(String text, int amount)
	{
		String tabs = "\t".repeat(amount);
		
		StringBuilder builder = new StringBuilder();
		
		Iterator<String> iterator = List.of(text.split("\n")).iterator();
		while (iterator.hasNext())
		{
			String line = iterator.next();
			builder.append(tabs);
			builder.append(line);
			if (iterator.hasNext())
			{
				builder.append("\n");
			}
		}
		
		return builder.toString();
	}
	
	public long longHashCode(String text)
	{
		long hash = 1125899906842597L; // A large prime number
		int len = text.length();
		
		for (int i = 0; i < len; i++)
		{
			hash = 31 * hash + text.charAt(i);
		}
		
		return hash;
	}
	
	public List<String> splitIntoChunks(String text, int maxLength, List<String> splitters)
	{
		List<String> result = new ArrayList<>();
		
		while (text.length() > maxLength)
		{
			StringSplitIndex index = StringSplitIndex.find(text, maxLength, splitters);
			String chunk = text.substring(0, index.getEnd());
			result.add(chunk);
			text = text.substring(index.getStart());
		}
		if (text.length() > 0)
		{
			result.add(text);
		}
		
		return result;
	}
	
	@Getter
	private static class StringSplitIndex
	{
		public static StringSplitIndex find(String text, int endIndex, List<String> splitters)
		{
			StringSplitIndex best = new StringSplitIndex(endIndex, "");
			for (String split : splitters)
			{
				int index = text.lastIndexOf(split, endIndex);
				if (best == null || (index != -1 && index <= best.getEnd()))
				{
					best = new StringSplitIndex(index, split);
				}
			}
			
			return best;
		}
		
		private final int end;
		private final String text;
		private final int start;
		
		public StringSplitIndex(int index, String text)
		{
			this.end = index;
			this.text = text;
			this.start = end + text.length();
		}
	}
	
	private static final Pattern formatPattern = Pattern.compile("\\{\\{([a-zA-Z0-9._-]+)\\}\\}");
	
	public String format(String template, Map<String, ? extends Object> variables)
	{
		return format(template, variables::get);
	}
	
	public String format(String template, VariableSource variableSource)
	{
		return formatPattern.matcher(template)
			.replaceAll(new VariableFormatter(variableSource));
	}
	
	public interface VariableSource
	{
		public Object get(String name);
	}
	
	@RequiredArgsConstructor
	private static class VariableFormatter implements Function<MatchResult, String>
	{
		private final VariableSource variableSource;
		
		private String getValue(String key)
		{
			Object value = variableSource.get(key);
			if (value == null)
			{
				throw new IllegalArgumentException("Missing variable: " + key);
			}
			return String.valueOf(value);
		}
		
		@Override
		public String apply(MatchResult match)
		{
			String key = match.group(1);
			return getValue(key);
		}
	}
}
