package endercrypt.library.commons.misc.loop;


import endercrypt.library.commons.misc.Ender;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.stream.Stream;


public class EnderLoop extends Ender
{
	public <E> Iterable<E> of(Enumeration<E> enumeration)
	{
		return of(enumeration.asIterator());
	}
	
	public <E> Iterable<E> of(Stream<E> stream)
	{
		return of(stream.iterator());
	}
	
	public <E> Iterable<E> of(Iterator<E> iterator)
	{
		return new Iterable<E>()
		{
			@Override
			public Iterator<E> iterator()
			{
				return iterator;
			}
		};
	}
}
