package endercrypt.library.commons.misc.time;


import endercrypt.library.commons.misc.Ender;

import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EnderTime extends Ender
{
	private static final Pattern pattern = Pattern.compile("(\\d+)([a-zA-Z])");
	private static final Map<Character, Long> units = new LinkedHashMap<>();
	
	static
	{
		// ORDER MATTERS
		units.put('d', 24L * 60L * 60L); // days
		units.put('h', 60L * 60L); // hours
		units.put('m', 60L); // minutes
		units.put('s', 1L); // seconds
	}
	
	public Duration describeToDuration(String text)
	{
		long seconds = 0;
		Matcher matcher = pattern.matcher(text);
		
		while (matcher.find())
		{
			long amount = Long.parseLong(matcher.group(1));
			char unit = Character.toLowerCase(matcher.group(2).charAt(0));
			Long multiplier = units.get(unit);
			if (multiplier == null)
			{
				throw new IllegalArgumentException("Invalid time unit: " + unit);
			}
			seconds += amount * multiplier;
		}
		
		return Duration.ofSeconds(seconds);
	}
	
	public String describeFromDuration(Duration duration)
	{
		long seconds = duration.getSeconds();
		return describeFromSeconds(seconds);
	}
	
	public String describeFromSeconds(long seconds)
	{
		StringBuilder result = new StringBuilder();
		
		for (Map.Entry<Character, Long> entry : units.entrySet())
		{
			char unit = entry.getKey();
			long secondsInUnit = entry.getValue();
			
			long amount = seconds / secondsInUnit;
			if (amount > 0)
			{
				result.append(amount).append(unit);
				seconds %= secondsInUnit;
			}
		}
		
		return result.toString();
	}
}
