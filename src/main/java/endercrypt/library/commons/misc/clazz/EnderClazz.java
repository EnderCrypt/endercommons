package endercrypt.library.commons.misc.clazz;


import endercrypt.library.commons.misc.Ender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


public class EnderClazz extends Ender
{
	public Class<?> getCallerClass()
	{
		return getCallerClass(2);
	}
	
	public Class<?> getCallerClass(int level)
	{
		if (level < 1)
		{
			throw new IllegalArgumentException("level cant be less than 1, but was " + level);
		}
		
		List<StackTraceElement> stack = getStackTrace();
		
		int index = 2 + level;
		if (index > stack.size())
		{
			throw new IllegalArgumentException("level " + level + " is out of range of the stack");
		}
		
		StackTraceElement frame = stack.get(index - 1);
		String qualifiedClassName = frame.getClassName();
		
		try
		{
			return Class.forName(qualifiedClassName);
		}
		catch (ClassNotFoundException e)
		{
			throw new IllegalArgumentException("Failed to extract caller class: " + qualifiedClassName);
		}
	}
	
	public String getHumanReadableClassName(Class<?> targetClass)
	{
		StringBuilder sb = new StringBuilder();
		for (char c : targetClass.getSimpleName().toCharArray())
		{
			if (sb.length() > 0 && c == Character.toUpperCase(c))
			{
				sb.append(" ");
			}
			sb.append(c);
		}
		return sb.toString();
	}
	
	public List<StackTraceElement> getStackTrace()
	{
		List<StackTraceElement> stack = getStackTrace(Thread.currentThread());
		stack.remove(0); // remove self call
		return stack;
	}
	
	public List<StackTraceElement> getStackTrace(Thread thread)
	{
		List<StackTraceElement> stack = new ArrayList<>(Arrays.asList(thread.getStackTrace()));
		
		// remove all irrelevant deeper stack traces
		Iterator<StackTraceElement> iterator = stack.iterator();
		while (iterator.hasNext())
		{
			StackTraceElement frame = iterator.next();
			iterator.remove();
			
			boolean classMatches = frame.getClassName().equals(getClass().getName());
			boolean methodMatches = frame.getMethodName().equals("getStackTrace");
			if (classMatches && methodMatches)
			{
				break;
			}
		}
		
		return stack;
	}
	
	public List<Class<?>> getClassHierarchy(Class<?> targetClass)
	{
		List<Class<?>> result = new ArrayList<>();
		while (targetClass != null)
		{
			result.add(targetClass);
			targetClass = targetClass.getSuperclass();
		}
		return result;
	}
	
	public List<Class<?>> getClassParents(Class<?> targetClass)
	{
		List<Class<?>> result = getClassHierarchy(targetClass);
		result.remove(targetClass);
		return result;
	}
}
