package endercrypt.library.commons.misc.array;


import endercrypt.library.commons.misc.Ender;


public class EnderArray extends Ender
{
	public byte[] ofBytes(int... ints)
	{
		byte[] result = new byte[ints.length];
		for (int i = 0; i < ints.length; i++)
		{
			int value = ints[i];
			if (value < 0 || value > 255)
			{
				throw new IllegalArgumentException("Index " + i + " of ints array with length " + ints.length + " is non-byte value " + value);
			}
			result[i] = (byte) value;
		}
		return result;
	}
}
