package endercrypt.library.commons.misc.math;


import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.position.Circle;

import java.math.RoundingMode;
import java.util.Arrays;


public class EnderMath extends Ender
{
	public double remap(double number, double fromLow, double fromHigh, double toLow, double toHigh)
	{
		double fromRange = fromHigh - fromLow;
		double toRange = toHigh - toLow;
		return (number - fromLow) / fromRange * toRange + toLow;
	}
	
	public long round(double value)
	{
		return (int) round(value, 0);
	}
	
	public double round(double value, int precision)
	{
		double power = (long) Math.pow(10, precision);
		return Math.round(value * power) / power;
	}
	
	public long round(double value, RoundingMode roundingMode)
	{
		return (long) roundWithMode(value, roundingMode);
	}
	
	private double roundWithMode(double value, RoundingMode roundingMode)
	{
		if (((long) value) == value)
			return value;
		
		switch (roundingMode)
		{
		case CEILING:
			return Math.ceil(value);
		
		case FLOOR:
			return Math.floor(value);
		
		case UP:
			return value + 1;
		
		case DOWN:
			return value;
		
		case HALF_UP:
			return value + 0.5;
		
		case HALF_DOWN:
			return value - 0.5;
		
		case HALF_EVEN:
			int up = (int) Math.ceil(value);
			int down = (int) Math.floor(value);
			return (up % 2 == 0) ? up : down;
		
		case UNNECESSARY:
			throw new ArithmeticException("unexpected decimal value: " + value);
		
		default:
			throw new IllegalArgumentException("Unknown rounding mode: " + roundingMode);
		}
	}
	
	/*
	 * x mod y behaving the same way as Math.floorMod but with doubles
	 * 
	 * credits to: https://stackoverflow.com/a/55205438
	 */
	public double floorMod(double x, double y)
	{
		return (x - Math.floor(x / y) * y);
	}
	
	public int randomRange(int min, int max)
	{
		return (int) Math.round(randomRange((double) min, (double) max));
	}
	
	public double randomRange(double min, double max)
	{
		return min + (Math.random() * (max - min));
	}
	
	public boolean randomBoolean()
	{
		return Math.random() < 0.5;
	}
	
	public int randomSign()
	{
		return randomBoolean() ? 1 : -1;
	}
	
	/**
	 * Credits to: https://stackoverflow.com/a/30887154
	 */
	public double angleDifference(double source, double target)
	{
		double d = Math.abs(source - target) % Circle.FULL;
		double r = d > Circle.HALF ? Circle.FULL - d : d;
		
		//calculate sign 
		int sign = (source - target >= 0 && source - target <= Circle.HALF) || (source - target <= -Circle.HALF && source - target >= -Circle.FULL) ? -1 : 1;
		r *= sign;
		return r;
	}
	
	public int clamp(int value, int min, int max)
	{
		value = Math.max(value, min);
		value = Math.min(value, max);
		return value;
	}
	
	public double clamp(double value, double min, double max)
	{
		value = Math.max(value, min);
		value = Math.min(value, max);
		return value;
	}
	
	public double nthRoot(double value, double nth)
	{
		return Math.pow(value, 1.0 / nth);
	}
	
	public double nthRoot(double value, double nth, int precision)
	{
		return round(nthRoot(value, nth), precision);
	}
	
	public int collapse(double value, double intensity)
	{
		int up = (int) Math.ceil(value);
		double upDifference = up - value;
		double upModifier = Math.pow(1 - upDifference, intensity);
		
		int down = (int) Math.floor(value);
		double downDifference = value - down;
		double downModifier = Math.pow(1 - downDifference, intensity);
		
		double choosen = Math.random() * (upModifier + downModifier);
		// System.out.println(Ender.math.round(upModifier, 5) + " vs " + Ender.math.round(downModifier, 5) + " --> " + Ender.math.round(choosen, 5));
		
		return choosen < upModifier ? up : down;
	}
	
	/**
	 * credits:
	 * https://stackoverflow.com/questions/13263948/fast-sqrt-in-java-at-the-expense-of-accuracy
	 */
	public double fastSqrt(double number)
	{
		final int magic = number <= 100_000 ? 1072632448 : 1072679338;
		return Double.longBitsToDouble(((Double.doubleToRawLongBits(number) >> 32) + magic) << 31);
	}
	
	public double fastInverseSqrt(double value)
	{
		double xhalf = 0.5d * value;
		long i = Double.doubleToLongBits(value);
		i = 0x5fe6ec85e7de30daL - (i >> 1);
		value = Double.longBitsToDouble(i);
		value *= (1.5d - xhalf * value * value);
		return value;
	}
	
	public double lerp(double start, double end, double progress)
	{
		double difference = end - start;
		double forward = difference * progress;
		return start + forward;
	}
	
	public double max(double... doubles)
	{
		return Arrays.stream(doubles)
			.reduce(Double.MIN_VALUE, Math::max);
	}
	
	public int max(int... ints)
	{
		return Arrays.stream(ints)
			.reduce(Integer.MIN_VALUE, Math::max);
	}
	
	public long max(long... longs)
	{
		return Arrays.stream(longs)
			.reduce(Long.MIN_VALUE, Math::max);
	}
	
	public double min(double... doubles)
	{
		return Arrays.stream(doubles)
			.reduce(Double.MAX_VALUE, Math::min);
	}
	
	public int min(int... ints)
	{
		return Arrays.stream(ints)
			.reduce(Integer.MAX_VALUE, Math::min);
	}
	
	public long min(long... longs)
	{
		return Arrays.stream(longs)
			.reduce(Long.MAX_VALUE, Math::min);
	}
}
