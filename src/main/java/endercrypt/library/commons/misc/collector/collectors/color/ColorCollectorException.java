package endercrypt.library.commons.misc.collector.collectors.color;

public class ColorCollectorException extends RuntimeException
{
	private static final long serialVersionUID = 5799563202340748672L;
	
	public ColorCollectorException(String message)
	{
		super(message);
	}
}
