package endercrypt.library.commons.misc.collector.collectors;


import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public abstract class AbstractEnderCollector<I, A extends AbstractEnderCollector.AbstractAccumulator<I, A, O>, O> implements Collector<I, A, O>
{
	protected abstract A createAccumulator();
	
	@Override
	public Supplier<A> supplier()
	{
		return this::createAccumulator;
	}
	
	@Override
	public BiConsumer<A, I> accumulator()
	{
		return AbstractAccumulator::onInput;
	}
	
	@Override
	public BinaryOperator<A> combiner()
	{
		return (acc1, acc2) -> acc1.createCombined(acc2);
	}
	
	@Override
	public Function<A, O> finisher()
	{
		return AbstractAccumulator::createOutput;
	}
	
	public interface AbstractAccumulator<I, A, O>
	{
		public void onInput(I input);
		
		public A createCombined(A other);
		
		public O createOutput();
	}
}
