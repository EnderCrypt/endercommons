package endercrypt.library.commons.misc.collector.collectors.adapter;


import java.util.function.BiConsumer;
import java.util.stream.Collector;

import lombok.experimental.Delegate;


public class InputAdapterCollector<I, T, A, R> implements Collector<I, A, R>
{
	private final Adapter<I, T> adapter;
	
	@Delegate
	private final Collector<T, A, R> collector;
	private BiConsumer<A, T> originalAdapterAccumulator;
	
	public InputAdapterCollector(Adapter<I, T> adapter, Collector<T, A, R> collector)
	{
		this.adapter = adapter;
		this.collector = collector;
		this.originalAdapterAccumulator = collector.accumulator();
	}
	
	@Override
	public BiConsumer<A, I> accumulator()
	{
		return new BiConsumer<A, I>()
		{
			@Override
			public void accept(A accumulator, I input)
			{
				T element = adapter.adapt(input);
				originalAdapterAccumulator.accept(accumulator, element);
			}
		};
	}
	
	public interface Adapter<I, T>
	{
		public T adapt(I input);
	}
}
