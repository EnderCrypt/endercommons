package endercrypt.library.commons.misc.collector.collectors.color;


import endercrypt.library.commons.misc.collector.collectors.AbstractEnderCollector;
import endercrypt.library.commons.misc.color.ColorChannel;
import endercrypt.library.commons.misc.color.ColorChannel.Value;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class ColorCollector extends AbstractEnderCollector<ColorChannel.Value, ColorCollector.Accumulator, Color>
{
	private final Color defaultColorValues;
	
	@Override
	public Set<Characteristics> characteristics()
	{
		return Set.of(Collector.Characteristics.UNORDERED);
	}
	
	@Override
	protected Accumulator createAccumulator()
	{
		return new Accumulator();
	}
	
	private ColorChannel.Value getDefaultColor(ColorChannel channel)
	{
		if (defaultColorValues == null)
		{
			throw new ColorCollectorException("no default color exists to supply " + channel);
		}
		return channel.extractAsValue(defaultColorValues);
	}
	
	public class Accumulator implements AbstractEnderCollector.AbstractAccumulator<ColorChannel.Value, ColorCollector.Accumulator, Color>
	{
		private final Map<ColorChannel, ColorChannel.Value> channels = new HashMap<>();
		
		@Override
		public void onInput(Value input)
		{
			channels.put(input.getChannel(), input);
		}
		
		@Override
		public Accumulator createCombined(Accumulator other)
		{
			Accumulator combined = new Accumulator();
			combined.channels.putAll(channels);
			combined.channels.putAll(other.channels);
			return combined;
		}
		
		private int getColor(ColorChannel channel)
		{
			Value value = channels.get(channel);
			if (value == null)
			{
				value = getDefaultColor(channel);
			}
			return value.asInteger();
		}
		
		@Override
		public Color createOutput()
		{
			int r = getColor(ColorChannel.RED);
			int g = getColor(ColorChannel.GREEN);
			int b = getColor(ColorChannel.BLUE);
			int a = getColor(ColorChannel.ALPHA);
			return new Color(r, g, b, a);
		}
	}
}
