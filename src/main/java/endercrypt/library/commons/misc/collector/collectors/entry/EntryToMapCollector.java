package endercrypt.library.commons.misc.collector.collectors.entry;


import endercrypt.library.commons.entry.Entry;
import endercrypt.library.commons.misc.collector.collectors.AbstractEnderCollector;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class EntryToMapCollector<K, V> extends AbstractEnderCollector<Entry<K, V>, EntryToMapCollector.Accumulator<K, V>, Map<K, V>>
{
	@Override
	public Set<Characteristics> characteristics()
	{
		return Set.of();
	}
	
	@Override
	protected Accumulator<K, V> createAccumulator()
	{
		return new Accumulator<>();
	}
	
	public static class Accumulator<K, V> implements AbstractEnderCollector.AbstractAccumulator<Entry<K, V>, EntryToMapCollector.Accumulator<K, V>, Map<K, V>>
	{
		private final Map<K, V> values = new HashMap<>();
		
		@Override
		public void onInput(Entry<K, V> input)
		{
			values.put(input.getKey(), input.getValue());
		}
		
		@Override
		public Accumulator<K, V> createCombined(Accumulator<K, V> other)
		{
			Accumulator<K, V> combined = new Accumulator<>();
			combined.values.putAll(this.values);
			combined.values.putAll(other.values);
			return combined;
		}
		
		@Override
		public Map<K, V> createOutput()
		{
			return new HashMap<>(values);
		}
	}
}
