package endercrypt.library.commons.misc.collector;


import endercrypt.library.commons.entry.Entry;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.misc.collector.collectors.adapter.InputAdapterCollector;
import endercrypt.library.commons.misc.collector.collectors.color.ColorCollector;
import endercrypt.library.commons.misc.collector.collectors.entry.EntryToMapCollector;
import endercrypt.library.commons.misc.color.ColorChannel;

import java.awt.Color;
import java.util.Map;
import java.util.stream.Collector;


public class EnderCollector extends Ender
{
	public Collector<ColorChannel.Value, ColorCollector.Accumulator, Color> forColors()
	{
		return forColors(null);
	}
	
	public Collector<ColorChannel.Value, ColorCollector.Accumulator, Color> forColors(Color defaultColorValues)
	{
		return new ColorCollector(defaultColorValues);
	}
	
	public <K, V> Collector<java.util.Map.Entry<K, V>, EntryToMapCollector.Accumulator<K, V>, Map<K, V>> forJdkMapEntries()
	{
		return new InputAdapterCollector<java.util.Map.Entry<K, V>, Entry<K, V>, EntryToMapCollector.Accumulator<K, V>, Map<K, V>>(Entry::immutable, forMapEntries());
	}
	
	public <K, V> Collector<Entry<K, V>, EntryToMapCollector.Accumulator<K, V>, Map<K, V>> forMapEntries()
	{
		return new EntryToMapCollector<>();
	}
}
