package endercrypt.library.commons.misc.files;


import endercrypt.library.commons.misc.Ender;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Getter;


public class EnderFiles extends Ender
{
	private static final String PATH_VARIABLE = System.getenv("PATH");
	
	@Getter
	private static final Set<Path> paths = Arrays.stream(PATH_VARIABLE.split(getPathSplitCharacter()))
		.map(Paths::get)
		.collect(Collectors.toSet());
	
	private static final String getPathSplitCharacter()
	{
		if (System.getProperty("os.name").startsWith("Windows"))
		{
			return ";";
		}
		
		return ":";
	}
	
	public Optional<Path> findExecutable(String filename)
	{
		return getPaths().stream()
			.map(directory -> directory.resolve(filename))
			.filter(Files::isExecutable)
			.findFirst();
	}
	
	public boolean isInvisible(Path path)
	{
		return path.getFileName().startsWith(".");
	}
	
	public Optional<String> getExtension(Path path)
	{
		return getExtension(path.getFileName().toString());
	}
	
	public Optional<String> getExtension(String filename)
	{
		int dotIndex = filename.lastIndexOf(".");
		if (dotIndex == -1)
		{
			return Optional.empty();
		}
		return Optional.of(filename.substring(dotIndex + 1));
	}
	
	public Optional<String> getBaseName(Path path)
	{
		return getBaseName(path.getFileName().toString());
	}
	
	public Optional<String> getBaseName(String filename)
	{
		int dotIndex = filename.lastIndexOf(".");
		if (dotIndex == -1)
		{
			return Optional.empty();
		}
		return Optional.of(filename.substring(0, dotIndex));
	}
}
