package endercrypt.library.commons.misc;


import endercrypt.library.commons.misc.array.EnderArray;
import endercrypt.library.commons.misc.clazz.EnderClazz;
import endercrypt.library.commons.misc.collector.EnderCollector;
import endercrypt.library.commons.misc.color.EnderColor;
import endercrypt.library.commons.misc.exception.EnderException;
import endercrypt.library.commons.misc.files.EnderFiles;
import endercrypt.library.commons.misc.format.EnderFormat;
import endercrypt.library.commons.misc.generics.EnderGenerics;
import endercrypt.library.commons.misc.graphics.EnderGraphics;
import endercrypt.library.commons.misc.loop.EnderLoop;
import endercrypt.library.commons.misc.math.EnderMath;
import endercrypt.library.commons.misc.parse.EnderParse;
import endercrypt.library.commons.misc.stream.EnderStream;
import endercrypt.library.commons.misc.text.EnderText;
import endercrypt.library.commons.misc.thread.EnderThread;
import endercrypt.library.commons.misc.time.EnderTime;
import endercrypt.library.commons.reject.ThrowRejectDueTo;

import java.util.function.Supplier;


public abstract class Ender
{
	protected Ender()
	{
		if (Ender.clazz != null && Ender.clazz.getCallerClass(3) != Ender.class)
		{
			ThrowRejectDueTo.forbiddenConstructionAttempt();
		}
	}
	
	private static <T extends Ender> T create(Supplier<T> creator)
	{
		try
		{
			return creator.get();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static final EnderClazz clazz = create(EnderClazz::new); // always first!
	
	public static final EnderArray array = create(EnderArray::new);
	
	public static final EnderCollector collector = create(EnderCollector::new);
	
	public static final EnderColor color = create(EnderColor::new);
	
	public static final EnderException exception = create(EnderException::new);
	
	public static final EnderFiles files = create(EnderFiles::new);
	
	public static final EnderFormat format = create(EnderFormat::new);
	
	public static final EnderGenerics generics = create(EnderGenerics::new);
	
	public static final EnderGraphics graphics = create(EnderGraphics::new);
	
	public static final EnderLoop loop = create(EnderLoop::new);
	
	public static final EnderMath math = create(EnderMath::new);
	
	public static final EnderParse parse = create(EnderParse::new);
	
	public static final EnderStream stream = create(EnderStream::new);
	
	public static final EnderText text = create(EnderText::new);
	
	public static final EnderThread thread = create(EnderThread::new);
	
	public static final EnderTime time = create(EnderTime::new);
}
