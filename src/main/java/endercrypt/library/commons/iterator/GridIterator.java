package endercrypt.library.commons.iterator;

public class GridIterator<E> extends AbstractIterator<E>
{
	private final int startX;
	private final int startY;
	private final int endX;
	private final int endY;
	private final Grid<E> grid;
	private int x;
	private int y;
	
	public GridIterator(int startX, int startY, int endX, int endY, Grid<E> grid)
	{
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
		this.grid = grid;
		
		this.x = startX;
		this.y = startY;
	}
	
	@Override
	protected Content<E> advance()
	{
		if (y >= endY)
		{
			return endOfContent();
		}
		E element = grid.get(x, y);
		x++;
		if (x >= endX)
		{
			x = startX;
			y++;
		}
		return nextContent(element);
	}
	
	public interface Grid<E>
	{
		public abstract E get(int x, int y);
	}
}
