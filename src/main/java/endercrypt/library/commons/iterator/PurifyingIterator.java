package endercrypt.library.commons.iterator;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import lombok.NonNull;


public class PurifyingIterator<E> extends AbstractIterator<E>
{
	private final Iterator<E> iterator;
	private final Predicate<E> condition;
	private final List<Consumer<E>> purifyListeners = new ArrayList<>();
	
	public PurifyingIterator(Iterable<E> iterable, Predicate<E> condition)
	{
		this(iterable.iterator(), condition);
	}
	
	public PurifyingIterator(@NonNull Iterator<E> iterator, @NonNull Predicate<E> condition)
	{
		this.iterator = iterator;
		this.condition = condition;
	}
	
	public PurifyingIterator<E> addPurifyListener(@NonNull Consumer<E> consumer)
	{
		purifyListeners.add(consumer);
		return this;
	}
	
	@Override
	protected Content<E> advance()
	{
		while (iterator.hasNext())
		{
			E element = iterator.next();
			if (condition.test(element))
			{
				return nextContent(element);
			}
			else
			{
				performPurify(element);
			}
		}
		return endOfContent();
	}
	
	private void performPurify(E element)
	{
		purifyListeners.forEach(listener -> listener.accept(element));
		iterator.remove();
	}
}
