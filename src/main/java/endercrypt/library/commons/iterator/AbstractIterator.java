package endercrypt.library.commons.iterator;


import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

import lombok.RequiredArgsConstructor;


public abstract class AbstractIterator<E> implements Iterator<E>
{
	private Content<E> content;
	
	protected abstract Content<E> advance();
	
	private Content<E> fetchContent()
	{
		if (content == null)
		{
			content = advance();
			if (content == null)
			{
				throw new NullPointerException(getClass().getName() + ".advance() returned null");
			}
		}
		return content;
	}
	
	@Override
	public boolean hasNext()
	{
		return fetchContent().hasNext();
	}
	
	@Override
	public E next()
	{
		E value = fetchContent().next();
		content = null;
		return value;
	}
	
	protected final static <E> Content<E> from(Iterator<E> iterator)
	{
		return iterator.hasNext()
			? nextContent(iterator.next())
			: endOfContent();
	}
	
	protected final static <E> Content<E> nextContent(E item)
	{
		return new Content<>(true, () -> item);
	}
	
	protected final static <E> Content<E> endOfContent()
	{
		return new Content<>(false, AbstractIterator::fail);
	}
	
	private static <E> E fail()
	{
		throw new NoSuchElementException();
	}
	
	@RequiredArgsConstructor
	public static class Content<E>
	{
		private final boolean next;
		
		private final Supplier<E> itemSupplier;
		
		public boolean hasNext()
		{
			return next;
		}
		
		public E next()
		{
			return itemSupplier.get();
		}
	}
}
