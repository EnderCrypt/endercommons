package endercrypt.library.commons.iterator;


import endercrypt.library.commons.iterator.AbstractIterator.Content;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class BranchableIterator<E> implements Iterator<E>
{
	public static <E> BranchableIterator<E> of(Iterator<E> iterator)
	{
		if (iterator instanceof BranchableIterator) // should be <E> instead of <?> but JDK 11 prevents it (requires 16)
		{
			return ((BranchableIterator<E>) iterator).branch();
		}
		return new BranchableIterator<>(new OriginalSource<E>(iterator));
	}
	
	private final Source<E> source;
	private int index = 0;
	
	public BranchableIterator<E> branch()
	{
		return new BranchableIterator<>(new DelegateSource<>(source, index));
	}
	
	@Override
	public boolean hasNext()
	{
		return source.get(index).hasNext();
	}
	
	@Override
	public E next()
	{
		return source.get(index++).next();
	}
	
	private interface Source<E>
	{
		public Content<E> get(int index);
	}
	
	@RequiredArgsConstructor
	private static class OriginalSource<E> implements Source<E>
	{
		private final Iterator<E> iterator;
		private final List<E> queue = new ArrayList<>();
		
		@Override
		public Content<E> get(int index)
		{
			if (index > queue.size())
			{
				return AbstractIterator.endOfContent();
			}
			while (queue.size() - 1 < index)
			{
				if (iterator.hasNext() == false)
				{
					return AbstractIterator.endOfContent();
				}
				queue.add(iterator.next());
			}
			return AbstractIterator.nextContent(queue.get(index));
		}
	}
	
	@RequiredArgsConstructor
	private static class DelegateSource<E> implements Source<E>
	{
		private final Source<E> source;
		private final int offset;
		
		@Override
		public Content<E> get(int index)
		{
			return source.get(offset + index);
		}
	}
}
