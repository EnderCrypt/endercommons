package endercrypt.library.commons.iterator;


import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class ArrayIterator<E> extends AbstractIterator<E>
{
	@NonNull
	private final E[] array;
	private int index = 0;
	
	@Override
	protected Content<E> advance()
	{
		if (index >= array.length)
		{
			return endOfContent();
		}
		return nextContent(array[index++]);
	}
}
