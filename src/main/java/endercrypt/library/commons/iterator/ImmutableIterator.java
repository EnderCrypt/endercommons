package endercrypt.library.commons.iterator;


import java.util.Iterator;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class ImmutableIterator<E> implements Iterator<E>
{
	@NonNull
	private final Iterator<E> iterator;
	
	@Override
	public boolean hasNext()
	{
		return iterator.hasNext();
	}
	
	@Override
	public E next()
	{
		return iterator.next();
	}
}
