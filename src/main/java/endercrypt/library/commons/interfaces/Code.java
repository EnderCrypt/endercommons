package endercrypt.library.commons.interfaces;


import java.util.function.Consumer;


@FunctionalInterface
public interface Code
{
	public static Runnable toRunnable(Code code, Consumer<Exception> errorHandler)
	{
		return new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					code.run();
				}
				catch (Exception e)
				{
					errorHandler.accept(e);
				}
			}
		};
	}
	
	public static Code fromRunnable(Runnable runnable)
	{
		return runnable::run;
	}
	
	public void run() throws Exception;
}
