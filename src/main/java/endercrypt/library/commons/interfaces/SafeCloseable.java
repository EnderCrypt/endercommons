package endercrypt.library.commons.interfaces;

public interface SafeCloseable extends AutoCloseable
{
	@Override
	public void close();
}
