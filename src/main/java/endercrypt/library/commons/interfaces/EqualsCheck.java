package endercrypt.library.commons.interfaces;


import java.util.function.Predicate;


@FunctionalInterface
public interface EqualsCheck<T>
{
	public static EqualsCheck<String> forString(boolean caseSensitive)
	{
		return caseSensitive ? String::equals : String::equalsIgnoreCase;
	}
	
	public static Predicate<String> stringPredicate(String string)
	{
		return stringPredicate(true, string);
	}
	
	public static Predicate<String> stringPredicate(boolean caseSensitive, String string)
	{
		return (other) -> forString(caseSensitive).isEquals(string, other);
	}
	
	boolean isEquals(T object1, T object2);
}
