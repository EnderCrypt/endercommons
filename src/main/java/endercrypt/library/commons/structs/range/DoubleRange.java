package endercrypt.library.commons.structs.range;


import endercrypt.library.commons.structs.number.EnderDouble;
import endercrypt.library.commons.structs.number.EnderNumber;


public class DoubleRange extends Range<Double, EnderDouble>
{
	protected DoubleRange(Double minimum, Double maximum)
	{
		super(minimum, maximum);
	}
	
	@Override
	protected EnderDouble toEnderNumber(Number value)
	{
		return EnderDouble.of((double) value);
	}
	
	@Override
	protected Double toJavaNumber(EnderNumber<?> value)
	{
		return value.doubleValue();
	}
}
