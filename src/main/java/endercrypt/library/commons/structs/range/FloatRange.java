package endercrypt.library.commons.structs.range;


import endercrypt.library.commons.structs.number.EnderFloat;
import endercrypt.library.commons.structs.number.EnderNumber;


public class FloatRange extends Range<Float, EnderFloat>
{
	protected FloatRange(Float minimum, Float maximum)
	{
		super(minimum, maximum);
	}
	
	@Override
	protected EnderFloat toEnderNumber(Number value)
	{
		return EnderFloat.of((float) value);
	}
	
	@Override
	protected Float toJavaNumber(EnderNumber<?> value)
	{
		return value.floatValue();
	}
}
