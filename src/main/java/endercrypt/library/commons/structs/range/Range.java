package endercrypt.library.commons.structs.range;


import endercrypt.library.commons.structs.number.EnderDouble;
import endercrypt.library.commons.structs.number.EnderNumber;
import endercrypt.library.commons.structs.number.EnderNumberMath;
import endercrypt.library.commons.structs.sign.Signs;


public abstract class Range<N extends Number, E extends EnderNumber<N>>
{
	public static IntRange of(int minimum, int maximum)
	{
		return new IntRange(minimum, maximum);
	}
	
	public static LongRange of(long minimum, long maximum)
	{
		return new LongRange(minimum, maximum);
	}
	
	public static FloatRange of(float minimum, float maximum)
	{
		return new FloatRange(minimum, maximum);
	}
	
	public static DoubleRange of(double minimum, double maximum)
	{
		return new DoubleRange(minimum, maximum);
	}
	
	private final E minimum;
	private final E maximum;
	
	protected Range(N minimum, N maximum)
	{
		E valueOne = toEnderNumber(minimum);
		E valueTwo = toEnderNumber(maximum);
		this.minimum = EnderNumberMath.min(valueOne, valueTwo);
		this.maximum = EnderNumberMath.max(valueOne, valueTwo);
	}
	
	public N getMinimum()
	{
		return minimum.getValue();
	}
	
	public N getMaximum()
	{
		return maximum.getValue();
	}
	
	private final E getEnderDelta()
	{
		return toEnderNumber(maximum.subtract(minimum));
	}
	
	public final N getDelta()
	{
		return toJavaNumber(getEnderDelta());
	}
	
	public final N getRandom()
	{
		E delta = getEnderDelta();
		EnderDouble part = delta.random();
		return toJavaNumber(minimum.add(part));
	}
	
	public final N getRandomSide()
	{
		return Signs.random().select(getMinimum(), getMaximum());
	}
	
	public final boolean isOutside(Number number)
	{
		return isInside(number) == false;
	}
	
	public final boolean isInside(Number number)
	{
		return isInside(EnderNumber.of(number));
	}
	
	private final boolean isInside(EnderNumber<?> number)
	{
		return number.isGreaterOrEqualTo(minimum) && number.isLessOrEqualTo(maximum);
	}
	
	protected E toEnderNumber(EnderNumber<?> value)
	{
		return toEnderNumber(toJavaNumber(value));
	}
	
	protected abstract E toEnderNumber(Number value);
	
	protected abstract N toJavaNumber(EnderNumber<?> value);
}
