package endercrypt.library.commons.structs.range;


import endercrypt.library.commons.structs.number.EnderInteger;
import endercrypt.library.commons.structs.number.EnderNumber;


public class IntRange extends Range<Integer, EnderInteger>
{
	protected IntRange(Integer minimum, Integer maximum)
	{
		super(minimum, maximum);
	}
	
	@Override
	protected EnderInteger toEnderNumber(Number value)
	{
		return EnderInteger.of((int) value);
	}
	
	@Override
	protected Integer toJavaNumber(EnderNumber<?> value)
	{
		return value.intValue();
	}
}
