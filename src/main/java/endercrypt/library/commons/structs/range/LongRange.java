package endercrypt.library.commons.structs.range;


import endercrypt.library.commons.structs.number.EnderLong;
import endercrypt.library.commons.structs.number.EnderNumber;


public class LongRange extends Range<Long, EnderLong>
{
	protected LongRange(Long minimum, Long maximum)
	{
		super(minimum, maximum);
	}
	
	@Override
	protected EnderLong toEnderNumber(Number value)
	{
		return EnderLong.of((long) value);
	}
	
	@Override
	protected Long toJavaNumber(EnderNumber<?> value)
	{
		return value.longValue();
	}
}
