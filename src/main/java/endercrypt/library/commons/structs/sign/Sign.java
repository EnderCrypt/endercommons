package endercrypt.library.commons.structs.sign;


import java.util.function.IntPredicate;

import lombok.Getter;


public class Sign
{
	@Getter
	private final String name;
	
	private final int intValue;
	
	private final IntPredicate intChecker;
	
	protected Sign(String name, int intValue, IntPredicate intChecker)
	{
		this.name = name;
		this.intValue = intValue;
		this.intChecker = intChecker;
		
		if (intValue < -1 || intValue > 1)
			throw new IllegalArgumentException(intValue + " is not a valid sign");
	}
	
	public boolean testInt(int value)
	{
		return intChecker.test(value);
	}
	
	public int asInt()
	{
		return intValue;
	}
	
	public boolean isZero()
	{
		return asInt() == 0;
	}
	
	public <T> T select(T positive, T zero, T negative)
	{
		switch (asInt())
		{
		case -1:
			return negative;
		case 0:
			return zero;
		case 1:
			return positive;
		default:
			throw new IllegalStateException("Unknown sign: " + this);
		}
	}
	
	@Override
	public String toString()
	{
		return getName();
	}
}
