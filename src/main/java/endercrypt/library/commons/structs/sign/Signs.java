package endercrypt.library.commons.structs.sign;


import endercrypt.library.commons.RandomEntry;

import java.util.List;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class Signs
{
	public static final NonZeroSign NEGATIVE = new NonZeroSign("NEGATIVE", false, -1, n -> n < 0);
	public static final Sign ZERO = new Sign("ZERO", 0, n -> n == 0);
	public static final NonZeroSign POSITIVE = new NonZeroSign("POSITIVE", true, +1, n -> n > 0);
	
	public static final List<Sign> allValues = List.of(NEGATIVE, ZERO, POSITIVE);
	public static final List<NonZeroSign> nonZeroValues = List.of(NEGATIVE, POSITIVE);
	
	public static Sign of(int value)
	{
		for (Sign sign : allValues)
		{
			if (sign.testInt(value))
			{
				return sign;
			}
		}
		throw new IllegalArgumentException("Impossible number supplied");
	}
	
	public static Sign of(boolean value)
	{
		for (NonZeroSign sign : nonZeroValues)
		{
			if (sign.testBoolean(value))
			{
				return sign;
			}
		}
		throw new IllegalArgumentException("Impossible boolean supplied");
	}
	
	public static NonZeroSign random()
	{
		return RandomEntry.from(nonZeroValues);
	}
	
	public static Sign randomWithZero()
	{
		return RandomEntry.from(allValues);
	}
}
