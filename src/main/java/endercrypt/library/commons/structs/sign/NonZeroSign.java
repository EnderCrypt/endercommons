package endercrypt.library.commons.structs.sign;


import java.util.function.IntPredicate;


public class NonZeroSign extends Sign
{
	private final boolean booleanValue;
	
	protected NonZeroSign(String name, boolean booleanValue, int intValue, IntPredicate intChecker)
	{
		super(name, intValue, intChecker);
		this.booleanValue = booleanValue;
	}
	
	public boolean testBoolean(boolean value)
	{
		return booleanValue == value;
	}
	
	public boolean asBoolean()
	{
		return booleanValue;
	}
	
	public <T> T select(T negative, T positive)
	{
		return select(negative, null, positive);
	}
}
