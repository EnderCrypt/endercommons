package endercrypt.library.commons.structs.number;


import endercrypt.library.commons.misc.Ender;

import java.math.RoundingMode;

import lombok.Getter;


@Getter
public class EnderInteger extends EnderNumberWhole<Integer>
{
	private static final long serialVersionUID = 8410266723705300204L;
	
	protected EnderInteger(Integer value)
	{
		super(value);
	}
	
	@Override
	public EnderLong round(RoundingMode roundingMode)
	{
		long result = Ender.math.round(value, roundingMode);
		return EnderLong.of(result);
	}
	
	@Override
	public EnderInteger add(EnderNumber<?> other)
	{
		return EnderInteger.of(value + other.intValue());
	}
	
	@Override
	public EnderInteger subtract(EnderNumber<?> other)
	{
		return EnderInteger.of(value - other.intValue());
	}
	
	@Override
	public EnderInteger divide(EnderNumber<?> other)
	{
		return EnderInteger.of(value / other.intValue());
	}
	
	@Override
	public EnderInteger multiply(EnderNumber<?> other)
	{
		return EnderInteger.of(value * other.intValue());
	}
}
