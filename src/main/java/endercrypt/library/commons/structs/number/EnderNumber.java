package endercrypt.library.commons.structs.number;


import endercrypt.library.commons.structs.sign.Sign;
import endercrypt.library.commons.structs.sign.Signs;

import java.math.RoundingMode;
import java.util.function.Supplier;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class EnderNumber<P extends Number> extends Number implements Comparable<EnderNumber<?>>
{
	private static final long serialVersionUID = -666183834556089469L;
	
	public static final EnderNumber<?> of(Number number)
	{
		if (number instanceof Integer)
			return of((int) number);
		
		if (number instanceof Long)
			return of((long) number);
		
		if (number instanceof Float)
			return of((float) number);
		
		if (number instanceof Double)
			return of((double) number);
		
		throw new IllegalArgumentException("Unsupported number type: " + number.getClass().getSimpleName());
	}
	
	public static final EnderInteger of(int value)
	{
		return new EnderInteger(value);
	}
	
	public static final EnderLong of(long value)
	{
		return new EnderLong(value);
	}
	
	public static final EnderFloat of(float value)
	{
		return new EnderFloat(value);
	}
	
	public static final EnderDouble of(double value)
	{
		return new EnderDouble(value);
	}
	
	@NonNull
	protected final P value;
	
	@Override
	public final int intValue()
	{
		return value.intValue();
	}
	
	@Override
	public long longValue()
	{
		return value.longValue();
	}
	
	@Override
	public final float floatValue()
	{
		return value.floatValue();
	}
	
	@Override
	public final double doubleValue()
	{
		return value.doubleValue();
	}
	
	public final EnderInteger asInt()
	{
		return as(EnderInteger.class, () -> EnderInteger.of(intValue()));
	}
	
	public final EnderLong asLong()
	{
		return as(EnderLong.class, () -> EnderLong.of(longValue()));
	}
	
	public final EnderFloat asFloat()
	{
		return as(EnderFloat.class, () -> EnderFloat.of(floatValue()));
	}
	
	public final EnderDouble asDouble()
	{
		return as(EnderDouble.class, () -> EnderDouble.of(doubleValue()));
	}
	
	@SuppressWarnings("unchecked")
	private final <N extends Number, T extends EnderNumber<N>> T as(Class<T> targetClass, Supplier<T> factory)
	{
		if (targetClass.isAssignableFrom(getClass()))
		{
			return (T) this;
		}
		return factory.get();
	}
	
	public final EnderNumberWhole<?> round()
	{
		return round(RoundingMode.HALF_UP);
	}
	
	public abstract EnderNumberWhole<?> round(RoundingMode roundingMode);
	
	public abstract EnderNumber<?> add(EnderNumber<?> other);
	
	public abstract EnderNumber<?> subtract(EnderNumber<?> other);
	
	public abstract EnderNumber<?> divide(EnderNumber<?> other);
	
	public abstract EnderNumber<?> multiply(EnderNumber<?> other);
	
	public final EnderDouble random()
	{
		return EnderDouble.of(doubleValue() * Math.random());
	}
	
	public final Sign getSign()
	{
		return Signs.of(intValue());
	}
	
	public final boolean isEqualTo(EnderNumber<?> other)
	{
		return compareTo(other) == 0;
	}
	
	public final boolean isGreaterThan(EnderNumber<?> other)
	{
		return compareTo(other) > 0;
	}
	
	public final boolean isGreaterOrEqualTo(EnderNumber<?> other)
	{
		return compareTo(other) >= 0;
	}
	
	public final boolean isLessThan(EnderNumber<?> other)
	{
		return compareTo(other) < 0;
	}
	
	public final boolean isLessOrEqualTo(EnderNumber<?> other)
	{
		return compareTo(other) <= 0;
	}
	
	@Override
	public int hashCode()
	{
		return Double.hashCode(doubleValue());
	}
	
	@Override
	public boolean equals(Object other)
	{
		if (this == other)
			return true;
		if (other instanceof EnderNumber<?> == false)
			return false;
		
		EnderNumber<?> otherNumber = (EnderNumber<?>) other;
		if (doubleValue() != otherNumber.doubleValue())
			return false;
		
		return true;
	}
	
	@Override
	public final int compareTo(EnderNumber<?> other)
	{
		return Double.compare(doubleValue(), other.doubleValue());
	}
	
	@Override
	public final String toString()
	{
		return String.valueOf(value);
	}
}
