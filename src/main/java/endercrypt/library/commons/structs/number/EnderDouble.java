package endercrypt.library.commons.structs.number;


import endercrypt.library.commons.misc.Ender;

import java.math.RoundingMode;

import lombok.Getter;


@Getter
public class EnderDouble extends EnderNumberDecimal<Double>
{
	private static final long serialVersionUID = -7831569765681465588L;
	
	protected EnderDouble(Double value)
	{
		super(value);
	}
	
	@Override
	public EnderLong round(RoundingMode roundingMode)
	{
		int result = (int) Ender.math.round(value, roundingMode);
		return EnderNumber.of(result).asLong();
	}
	
	@Override
	public EnderDouble add(EnderNumber<?> other)
	{
		return EnderDouble.of(value + other.doubleValue());
	}
	
	@Override
	public EnderDouble subtract(EnderNumber<?> other)
	{
		return EnderDouble.of(value - other.doubleValue());
	}
	
	@Override
	public EnderDouble divide(EnderNumber<?> other)
	{
		return EnderDouble.of(value / other.doubleValue());
	}
	
	@Override
	public EnderDouble multiply(EnderNumber<?> other)
	{
		return EnderDouble.of(value * other.doubleValue());
	}
}
