package endercrypt.library.commons.structs.number;


import endercrypt.library.commons.misc.Ender;

import java.math.RoundingMode;

import lombok.Getter;


@Getter
public class EnderFloat extends EnderNumberDecimal<Float>
{
	private static final long serialVersionUID = 764852559626159050L;

	protected EnderFloat(Float value)
	{
		super(value);
	}
	
	@Override
	public EnderLong round(RoundingMode roundingMode)
	{
		int result = (int) Ender.math.round(value, roundingMode);
		return EnderNumber.of(result).asLong();
	}
	
	@Override
	public EnderFloat add(EnderNumber<?> other)
	{
		return EnderFloat.of(value + other.floatValue());
	}
	
	@Override
	public EnderFloat subtract(EnderNumber<?> other)
	{
		return EnderFloat.of(value - other.floatValue());
	}
	
	@Override
	public EnderFloat divide(EnderNumber<?> other)
	{
		return EnderFloat.of(value / other.floatValue());
	}
	
	@Override
	public EnderFloat multiply(EnderNumber<?> other)
	{
		return EnderFloat.of(value * other.floatValue());
	}
}
