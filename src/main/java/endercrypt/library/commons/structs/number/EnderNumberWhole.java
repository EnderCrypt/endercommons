package endercrypt.library.commons.structs.number;

public abstract class EnderNumberWhole<P extends Number> extends EnderNumber<P>
{
	private static final long serialVersionUID = 8126124376684292009L;
	
	protected EnderNumberWhole(P value)
	{
		super(value);
	}
	
	@Override
	public abstract EnderNumberWhole<P> add(EnderNumber<?> other);
	
	@Override
	public abstract EnderNumberWhole<P> subtract(EnderNumber<?> other);
	
	@Override
	public abstract EnderNumberWhole<P> divide(EnderNumber<?> other);
	
	@Override
	public abstract EnderNumberWhole<P> multiply(EnderNumber<?> other);
}
