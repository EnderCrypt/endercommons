package endercrypt.library.commons.structs.number;

public abstract class EnderNumberDecimal<P extends Number> extends EnderNumber<P>
{
	private static final long serialVersionUID = 4357121873055800200L;
	
	protected EnderNumberDecimal(P value)
	{
		super(value);
	}
	
	@Override
	public abstract EnderNumberDecimal<P> add(EnderNumber<?> other);
	
	@Override
	public abstract EnderNumberDecimal<P> subtract(EnderNumber<?> other);
	
	@Override
	public abstract EnderNumberDecimal<P> divide(EnderNumber<?> other);
	
	@Override
	public abstract EnderNumberDecimal<P> multiply(EnderNumber<?> other);
}
