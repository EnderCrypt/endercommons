package endercrypt.library.commons.structs.number;


import endercrypt.library.commons.misc.Ender;

import java.math.RoundingMode;

import lombok.Getter;


@Getter
public class EnderLong extends EnderNumberWhole<Long>
{
	private static final long serialVersionUID = -3299837834107831285L;
	
	protected EnderLong(Long value)
	{
		super(value);
	}
	
	@Override
	public EnderLong round(RoundingMode roundingMode)
	{
		long result = Ender.math.round(value, roundingMode);
		return EnderLong.of(result);
	}
	
	@Override
	public EnderLong add(EnderNumber<?> other)
	{
		return EnderLong.of(value + other.longValue());
	}
	
	@Override
	public EnderLong subtract(EnderNumber<?> other)
	{
		return EnderLong.of(value - other.longValue());
	}
	
	@Override
	public EnderLong divide(EnderNumber<?> other)
	{
		return EnderLong.of(value / other.longValue());
	}
	
	@Override
	public EnderLong multiply(EnderNumber<?> other)
	{
		return EnderLong.of(value * other.longValue());
	}
}
