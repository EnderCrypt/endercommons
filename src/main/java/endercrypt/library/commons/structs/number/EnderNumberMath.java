package endercrypt.library.commons.structs.number;


import java.util.Comparator;
import java.util.stream.Stream;


public class EnderNumberMath
{
	@SafeVarargs
	public static <N extends Number, E extends EnderNumber<N>> E min(E... numbers)
	{
		return findEdge(Comparator.naturalOrder(), numbers);
	}
	
	@SafeVarargs
	public static <N extends Number, E extends EnderNumber<N>> E max(E... numbers)
	{
		return findEdge(Comparator.reverseOrder(), numbers);
	}
	
	public static <N extends Number, E extends EnderNumber<N>> E findEdge(Comparator<E> comparator, E... numbers)
	{
		if (numbers.length == 0)
			throw new IllegalArgumentException("no numbers provided");
		
		return Stream.of(numbers)
			.sorted(comparator)
			.findFirst()
			.orElseThrow();
	}
}
