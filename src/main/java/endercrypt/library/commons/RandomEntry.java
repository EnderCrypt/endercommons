package endercrypt.library.commons;


import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.reject.ThrowRejectDueTo;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class RandomEntry
{
	private static final Random random = new Random();
	
	private RandomEntry()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
	
	// INDEX //
	
	private static int generateRandomIndex(int size)
	{
		return random.nextInt(size);
	}
	
	private static <T> int generateRandomIndex(Collection<T> collection)
	{
		return generateRandomIndex(collection.size());
	}
	
	private static <T> int generateRandomIndex(T[] array)
	{
		return generateRandomIndex(array.length);
	}
	
	// STREAM //
	
	public static <T> T from(Stream<T> stream)
	{
		return from(stream.collect(Collectors.toList()));
	}
	
	private static <T> T from(int size, Stream<T> stream)
	{
		if (size == 0)
		{
			return null;
		}
		return stream
			.skip(generateRandomIndex(size))
			.findFirst()
			.get();
	}
	
	// ITERATOR //
	
	public static <T> T from(Iterable<T> iterable)
	{
		return from(Ender.stream.from(iterable));
	}
	
	public static <T> T from(Iterator<T> iterator)
	{
		return from(Ender.stream.from(iterator));
	}
	
	public static <T> T from(Spliterator<T> spliterator)
	{
		return from(Ender.stream.from(spliterator));
	}
	
	// ENUM //
	
	public static <T extends Enum<T>> T from(Class<T> enumClass)
	{
		return from(enumClass.getEnumConstants());
	}
	
	// COLLECTION //
	
	public static <T> T from(Collection<T> collection)
	{
		return from(collection.size(), collection.stream());
	}
	
	public static <K, V> Entry<K, V> from(Map<K, V> map)
	{
		return from(map.size(), map.entrySet().stream());
	}
	
	public static <T> T from(List<T> list)
	{
		if (list.isEmpty())
		{
			return null;
		}
		int index = generateRandomIndex(list);
		return list.get(index);
	}
	
	// ARRAY //
	
	@SuppressWarnings("unchecked")
	public static <T> T of(T... elements)
	{
		return from(elements);
	}
	
	public static <T> T from(T[] array)
	{
		if (array.length == 0)
		{
			return null;
		}
		int index = generateRandomIndex(array);
		return array[index];
	}
}
