package endercrypt.library.commons.tricks;


import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;


public class StreamTricks
{
	/**
	 * <code>
	 * Stream.of<Animal>(dog, cat, rat)
	 * 	.flatMap(StreamTricks.castInto(Cat.class));
	 * </code>
	 * 
	 * results in a stream containing only Cat.class and subclasses
	 */
	public static <I, R> Function<? super I, ? extends Stream<? extends R>> castInto(Class<R> targetClass)
	{
		return new Function<I, Stream<? extends R>>()
		{
			@Override
			public Stream<? extends R> apply(I object)
			{
				return Stream.of(object)
					.filter(targetClass::isInstance)
					.map(targetClass::cast);
			}
		};
	}
	
	/**
	 * <code>
	 * Stream.of(Optional.of("hello"), Optional.empty(), Optional.of("world"))
	 * 	.flatMap(StreamTricks.unwrapOptional());
	 * </code>
	 * 
	 * results in a stream containing the strings, with any empty optional
	 * ignored
	 */
	public static <T> Function<Optional<? super T>, ? extends Stream<? extends T>> unwrapOptional()
	{
		return new Function<Optional<? super T>, Stream<? extends T>>()
		{
			@SuppressWarnings("unchecked")
			@Override
			public Stream<? extends T> apply(Optional<? super T> optional)
			{
				return (Stream<? extends T>) optional.stream();
			}
		};
	}
	
	/**
	 * <code>
	 * Stream.empty()
	 * 	.findFirst()
	 * 	.orElseThrow(StreamTricks.throwWithMessage(RuntimeException::new, "no"))
	 * </code>
	 * 
	 * results in throwing a RuntimeException with the message "no"
	 */
	public static <T extends Throwable> Supplier<T> throwWithMessage(Function<String, T> exceptionFactory, String message)
	{
		return new Supplier<T>()
		{
			@Override
			public T get()
			{
				return exceptionFactory.apply(message);
			}
		};
	}
}
