package endercrypt.library.commons.data.extra;

public interface ByteLoop
{
	public static ByteLoop bytes(ByteValues bytesValues)
	{
		return new ByteLoop()
		{
			@Override
			public void process(int index, int value)
			{
				bytesValues.next(value);
			}
		};
	}
	
	public interface ByteValues
	{
		public void next(int value);
	}
	
	public static ByteLoop bytesAndPosition(BytePositionValues bytePositionValues)
	{
		return new ByteLoop()
		{
			@Override
			public void process(int value, int position)
			{
				bytePositionValues.next(value, position);
			}
		};
	}
	
	public interface BytePositionValues
	{
		public void next(int value, int position);
	}
	
	public void process(int value, int position);
}
