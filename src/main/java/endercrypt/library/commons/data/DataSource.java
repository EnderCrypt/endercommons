package endercrypt.library.commons.data;


import endercrypt.library.commons.data.sources.DiskDataReader;
import endercrypt.library.commons.data.sources.ResourcesDataReader;
import endercrypt.library.commons.misc.Ender;

import java.nio.file.Path;
import java.nio.file.Paths;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@NonNull
public enum DataSource
{
	DISK(DiskDataReader::new),
	RESOURCES(ResourcesDataReader::new);
	
	private final DataReaderFactory dataReaderFactory;
	
	public DataReader read(String path)
	{
		return dataReaderFactory.open(Ender.clazz.getCallerClass(), Paths.get(path));
	}
	
	public DataReader read(Path path)
	{
		return dataReaderFactory.open(Ender.clazz.getCallerClass(), path);
	}
	
	public DataReader read(Class<?> moduleClass, String path)
	{
		return read(moduleClass, Paths.get(path));
	}
	
	public DataReader read(Class<?> moduleClass, Path path)
	{
		return dataReaderFactory.open(moduleClass, path);
	}
}
