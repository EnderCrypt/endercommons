package endercrypt.library.commons.data;


import endercrypt.library.commons.data.extra.ByteLoop;
import endercrypt.library.commons.extended.graphics.SmartImageGraphics;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.CRC32;

import javax.imageio.ImageIO;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@NonNull
public abstract class DataReader
{
	protected final Class<?> moduleClass;
	protected final Path path;
	
	protected abstract InputStream createRawInputStream() throws IOException;
	
	public InputStream asInputStream() throws IOException
	{
		return new BufferedInputStream(createRawInputStream());
	}
	
	public byte[] asBytes() throws IOException
	{
		try (InputStream input = asInputStream())
		{
			return input.readAllBytes();
		}
	}
	
	public ByteArrayInputStream asInMemoryInputStream() throws IOException
	{
		return new ByteArrayInputStream(asBytes());
	}
	
	public String asString() throws IOException
	{
		return asString(StandardCharsets.UTF_8);
	}
	
	public String asString(Charset charset) throws IOException
	{
		return new String(asBytes(), charset);
	}
	
	public BufferedImage asImage() throws IOException
	{
		try (InputStream input = asInputStream())
		{
			return ImageIO.read(input);
		}
	}
	
	public SmartImageGraphics asSmartGraphics() throws IOException
	{
		return new SmartImageGraphics(asImage());
	}
	
	public void forEachByte(ByteLoop byteLoop) throws FileNotFoundException, IOException
	{
		try (InputStream stream = asInputStream())
		{
			int position = 0;
			int value;
			while ((value = stream.read()) != -1)
			{
				byteLoop.process(value, position);
				position++;
			}
		}
	}
	
	public long crc32() throws FileNotFoundException, IOException
	{
		CRC32 crc32 = new CRC32();
		forEachByte(ByteLoop.bytes(crc32::update));
		return crc32.getValue();
	}
	
	public DataReaderEntries splitNewline() throws IOException
	{
		return split("\n");
	}
	
	public DataReaderEntries split(String regex) throws IOException
	{
		return new DataReaderEntries(asString().split(regex));
	}
	
	@Getter
	@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
	public class DataReaderEntries implements Iterable<String>
	{
		private final String[] values;
		
		public String[] array()
		{
			return values;
		}
		
		public Stream<String> stream()
		{
			return Arrays.stream(values);
		}
		
		public <R, A> R collect(Collector<String, A, R> collector)
		{
			return stream().collect(collector);
		}
		
		public Set<String> set()
		{
			return collect(Collectors.toSet());
		}
		
		public List<String> list()
		{
			return collect(Collectors.toList());
		}
		
		@Override
		public Iterator<String> iterator()
		{
			return list().iterator();
		}
		
		public int count()
		{
			return array().length;
		}
	}
}
