package endercrypt.library.commons.data.sources;


import endercrypt.library.commons.data.DataReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;


public class ResourcesDataReader extends DataReader
{
	public ResourcesDataReader(Class<?> moduleClass, Path path)
	{
		super(moduleClass, path);
	}
	
	@Override
	public InputStream createRawInputStream() throws IOException
	{
		String filePath = Paths.get("/").resolve(path).normalize().toString().replace("\\", "/");
		
		InputStream inputStream = moduleClass.getResourceAsStream(filePath);
		if (inputStream == null)
		{
			throw new FileNotFoundException(filePath);
		}
		return inputStream;
	}
}
