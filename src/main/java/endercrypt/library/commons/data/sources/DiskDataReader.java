package endercrypt.library.commons.data.sources;


import endercrypt.library.commons.data.DataReader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;


public class DiskDataReader extends DataReader
{
	public DiskDataReader(Class<?> moduleClass, Path path)
	{
		super(moduleClass, path);
	}
	
	@Override
	public InputStream createRawInputStream() throws IOException
	{
		return new FileInputStream(path.toFile());
	}
}
