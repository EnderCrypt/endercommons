package endercrypt.library.commons.data;


import java.nio.file.Path;


interface DataReaderFactory
{
	public DataReader open(Class<?> moduleClass, Path path);
}
