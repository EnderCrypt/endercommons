package endercrypt.library.commons.randomizer;


import lombok.Getter;


@Getter
public class Ticket<T>
{
	private final double chance;
	private final T item;
	
	protected Ticket(double chance, T item)
	{
		if (chance <= 0)
		{
			throw new IllegalArgumentException("chance cannot be " + chance);
		}
		
		this.chance = chance;
		this.item = item;
	}
}
