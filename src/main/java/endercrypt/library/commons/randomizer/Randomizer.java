package endercrypt.library.commons.randomizer;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import lombok.NonNull;


public class Randomizer<T> implements Iterable<Ticket<T>>
{
	private final List<Ticket<T>> entries = new ArrayList<>();
	private double total = 0.0;
	
	public T pull()
	{
		double index = Math.random() * total;
		
		Iterator<Ticket<T>> iterator = iterator();
		while (iterator.hasNext())
		{
			Ticket<T> ticket = iterator.next();
			index -= ticket.getChance();
			if (index <= 0)
			{
				return ticket.getItem();
			}
		}
		throw new NoSuchElementException("failed to pull ticket");
	}
	
	public void enter(double chance, T item)
	{
		enter(new Ticket<>(chance, item));
	}
	
	private void enter(@NonNull Ticket<T> ticket)
	{
		entries.add(ticket);
		total += ticket.getChance();
	}
	
	@Override
	public Iterator<Ticket<T>> iterator()
	{
		return Collections.unmodifiableList(entries).iterator();
	}
}
