package endercrypt.library.commons;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class JarInfo
{
	public static final boolean isAndroid = "The Android Project".equals(System.getProperty("java.vendor"));
	
	public static final Path workingDirectory = Paths.get(System.getProperty("user.dir"));
	
	public static JarInfo of(Class<?> targetClass)
	{
		Path path = Paths.get(targetClass.getProtectionDomain().getCodeSource().getLocation().getPath().replace(":", ""));
		Path directory = path.getParent();
		String filename = Optional.ofNullable(path.getFileName()).map(Path::toString).orElse(null);
		return new JarInfo(path, directory, filename);
	}
	
	private final Path path;
	private final Path directory;
	private final String filename;
}
