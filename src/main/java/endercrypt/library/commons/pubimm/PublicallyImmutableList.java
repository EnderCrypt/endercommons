package endercrypt.library.commons.pubimm;


import java.util.ArrayList;
import java.util.List;


public class PublicallyImmutableList<T> extends PublicallyImmutableCollection<List<T>, T>
{
	private static final long serialVersionUID = 4547575097807620329L;
	
	@Override
	protected final List<T> createCollection()
	{
		return new ArrayList<>();
	}
	
	@Override
	public final List<T> getAll()
	{
		return List.copyOf(elements);
	}
}
