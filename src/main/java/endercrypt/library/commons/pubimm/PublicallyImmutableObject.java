package endercrypt.library.commons.pubimm;


import java.io.Serializable;
import java.util.Iterator;
import java.util.stream.Stream;


public abstract class PublicallyImmutableObject<C, T> implements Iterable<T>, Serializable
{
	private static final long serialVersionUID = 3290773135645053435L;
	
	protected final C elements = createCollection();
	
	protected abstract C createCollection();
	
	public abstract C getAll();
	
	public abstract Stream<T> stream();
	
	@Override
	public final Iterator<T> iterator()
	{
		return stream().iterator();
	}
	
	@Override
	public String toString()
	{
		return elements.toString();
	}
}
