package endercrypt.library.commons.pubimm;


import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Stream;


public class PublicallyImmutableMap<K, V> extends PublicallyImmutableObject<Map<K, V>, Entry<K, V>>
{
	private static final long serialVersionUID = -5779247952931020496L;
	
	@Override
	protected Map<K, V> createCollection()
	{
		return new HashMap<>();
	}
	
	@Override
	public Map<K, V> getAll()
	{
		return Collections.unmodifiableMap(elements);
	}
	
	public Set<K> getAllKeys()
	{
		return Collections.unmodifiableSet(elements.keySet());
	}
	
	public Collection<V> getAllValues()
	{
		return Collections.unmodifiableCollection(elements.values());
	}
	
	@Override
	public Stream<Entry<K, V>> stream()
	{
		return elements.entrySet().stream();
	}
	
	public Stream<K> streamKeys()
	{
		return elements.keySet().stream();
	}
	
	public Stream<V> streamValues()
	{
		return elements.values().stream();
	}
}
