package endercrypt.library.commons.pubimm;


import java.util.HashSet;
import java.util.Set;


public class PublicallyImmutableSet<T> extends PublicallyImmutableCollection<Set<T>, T>
{
	private static final long serialVersionUID = -8307212739830989247L;
	
	@Override
	protected final Set<T> createCollection()
	{
		return new HashSet<>();
	}
	
	@Override
	public final Set<T> getAll()
	{
		return Set.copyOf(elements);
	}
}
