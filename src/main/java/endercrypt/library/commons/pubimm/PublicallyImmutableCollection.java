package endercrypt.library.commons.pubimm;


import java.util.Collection;
import java.util.stream.Stream;


public abstract class PublicallyImmutableCollection<C extends Collection<T>, T> extends PublicallyImmutableObject<C, T>
{
	private static final long serialVersionUID = 7234181139349554870L;
	
	@Override
	public Stream<T> stream()
	{
		return elements.stream();
	}
}
