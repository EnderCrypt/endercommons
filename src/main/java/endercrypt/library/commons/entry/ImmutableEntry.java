package endercrypt.library.commons.entry;

public class ImmutableEntry<K, V> extends Entry<K, V>
{
	private static final long serialVersionUID = 3830137630443320984L;
	
	/**
	 * 
	 */
	
	private final K key;
	private final V value;
	
	public ImmutableEntry(K key, V value)
	{
		this.key = key;
		this.value = value;
	}
	
	@Override
	public K getKey()
	{
		return key;
	}
	
	@Override
	public V getValue()
	{
		return value;
	}
}
