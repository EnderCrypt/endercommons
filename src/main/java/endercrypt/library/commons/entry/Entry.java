package endercrypt.library.commons.entry;


import java.io.Serializable;
import java.util.Map;


public abstract class Entry<K, V> implements Serializable
{
	private static final long serialVersionUID = -7326470692562077198L;
	
	/**
	 * 
	 */
	
	public static <K, V> Map.Entry<K, V> mapEntry(Entry<K, V> entry)
	{
		return mapEntry(entry.getKey(), entry.getValue());
	}
	
	public static <K, V> Map.Entry<K, V> mapEntry(K key, V value)
	{
		return mutable(key, value);
	}
	
	public static <K, V> MutableEntry<K, V> mutable(Map.Entry<K, V> entry)
	{
		return mutable(entry.getKey(), entry.getValue());
	}
	
	public static <K, V> MutableEntry<K, V> mutable(K key, V value)
	{
		return new MutableEntry<>(key, value);
	}
	
	public static <K, V> ImmutableEntry<K, V> immutable(Map.Entry<K, V> entry)
	{
		return immutable(entry.getKey(), entry.getValue());
	}
	
	public static <K, V> ImmutableEntry<K, V> immutable(K key, V value)
	{
		return new ImmutableEntry<>(key, value);
	}
	
	public abstract K getKey();
	
	public abstract V getValue();
	
	/**
	 * should adhere to the implementation details for
	 * {@link Map.Entry#hashCode()}
	 */
	@Override
	public int hashCode()
	{
		K key = getKey();
		int keyHashCode = (key == null ? 0 : key.hashCode());
		V value = getValue();
		int valueHashCode = (value == null ? 0 : value.hashCode());
		return keyHashCode ^ valueHashCode;
	}
	
	/**
	 * should adhere to the implementation details for
	 * {@link Map.Entry#equals(Object)}
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof Entry<?, ?>)) return false;
		Entry<?, ?> other = (Entry<?, ?>) obj;
		K key = getKey();
		Object otherKey = other.getKey();
		if (key == null)
		{
			if (otherKey != null) return false;
		}
		else if (!key.equals(otherKey)) return false;
		V value = getValue();
		Object otherValue = other.getValue();
		if (value == null)
		{
			if (otherValue != null) return false;
		}
		else if (!value.equals(otherValue)) return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		return getClass() + "[key=" + getKey() + ", value=" + getValue() + "]";
	}
}
