package endercrypt.library.commons.entry;


import java.util.Map;


public class MutableEntry<K, V> extends Entry<K, V> implements Map.Entry<K, V>
{
	private static final long serialVersionUID = 7904993869162509631L;
	
	/**
	 * 
	 */
	
	private final K key;
	private V value;
	
	public MutableEntry(K key, V value)
	{
		this.key = key;
		this.value = value;
	}
	
	@Override
	public K getKey()
	{
		return key;
	}
	
	@Override
	public V getValue()
	{
		return value;
	}
	
	@Override
	public V setValue(V value)
	{
		V old = this.value;
		this.value = value;
		return old;
	}
}
