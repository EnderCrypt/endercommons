package endercrypt.library.commons.listener;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lombok.Getter;


public class ListenerCollection<T> implements Iterable<T>
{
	private final Class<T> listenerClass;
	
	@Getter
	private final T trigger;
	
	private final List<T> listeners = new ArrayList<>();
	
	public ListenerCollection(Class<T> listenerClass)
	{
		if (listenerClass.isInterface() == false)
		{
			throw new IllegalArgumentException("listenerClass must be an interface");
		}
		this.listenerClass = listenerClass;
		this.trigger = constructTriggerClass();
	}
	
	@SuppressWarnings("unchecked")
	private T constructTriggerClass()
	{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		Class<?>[] interfaces = new Class[] { listenerClass };
		InvocationHandler handler = new ListenerInvocationHandler();
		return (T) Proxy.newProxyInstance(classLoader, interfaces, handler);
	}
	
	public void add(T listener)
	{
		if (listeners.contains(listener) == false)
		{
			listeners.add(listener);
		}
	}
	
	public boolean remove(T listener)
	{
		return listeners.remove(listener);
	}
	
	@Override
	public Iterator<T> iterator()
	{
		return List.copyOf(listeners).iterator();
	}
	
	private class ListenerInvocationHandler implements InvocationHandler
	{
		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
		{
			for (T listener : listeners)
			{
				method.invoke(listener, args);
			}
			return null;
		}
	}
}
