package endercrypt.library.commons.trait;


import java.util.Arrays;
import java.util.Optional;


public interface Trait
{
	public static Optional<Class<?>> findTraitClass(Class<?> targetClass)
	{
		if (isHasTraitSuperClass(targetClass))
			return Optional.of(targetClass);
		
		return Arrays.stream(targetClass.getInterfaces())
			.map(Trait::findTraitClass)
			.flatMap(Optional::stream)
			.findFirst();
	}
	
	public static boolean isHasTraitSuperClass(Class<?> targetClass)
	{
		if (targetClass.isInterface() == false)
		{
			return false; // unnecessary check, but forces Trait's to always be interface classes
		}
		
		return Arrays.stream(targetClass.getInterfaces())
			.anyMatch(Trait.class::equals);
	}
	
	public static void verifyValidTraitClass(Class<?> targetClass)
	{
		if (Trait.isHasTraitSuperClass(targetClass) == false)
		{
			throw new TraitException(targetClass + " is not a valid trait class");
		}
	}
}
