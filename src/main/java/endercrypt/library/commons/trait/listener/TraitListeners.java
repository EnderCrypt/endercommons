package endercrypt.library.commons.trait.listener;


import endercrypt.library.commons.listener.ListenerCollection;

import lombok.Getter;


@Getter
public class TraitListeners
{
	private final ListenerCollection<TraitInsertListener> insert = new ListenerCollection<>(TraitInsertListener.class);
}
