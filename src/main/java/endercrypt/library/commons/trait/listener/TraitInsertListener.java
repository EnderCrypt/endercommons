package endercrypt.library.commons.trait.listener;

import endercrypt.library.commons.trait.Trait;

public interface TraitInsertListener
{
	public void onInsert(Trait trait);
}
