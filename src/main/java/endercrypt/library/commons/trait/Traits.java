package endercrypt.library.commons.trait;


import endercrypt.library.commons.trait.listener.TraitListeners;
import endercrypt.library.commons.trait.phantom.PhantomTraitFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class Traits
{
	private final Map<Class<?>, Trait> traits = new HashMap<>();
	
	@Getter
	private final TraitListeners listeners = new TraitListeners();
	
	public void set(Trait trait)
	{
		Class<?> traitClass = TraitLookup.getTraitClass(trait.getClass());
		if (traits.containsKey(traitClass))
		{
			throw new TraitException("Cannot set trait " + traitClass + " as its already set");
		}
		
		replace(trait);
	}
	
	public void replace(Trait trait)
	{
		Class<?> traitClass = TraitLookup.getTraitClass(trait.getClass());
		listeners.getInsert().getTrigger().onInsert(trait);
		traits.put(traitClass, trait);
	}
	
	public boolean has(Class<? extends Trait> traitClass)
	{
		Trait.verifyValidTraitClass(traitClass);
		return traits.containsKey(traitClass);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Trait> Optional<T> get(Class<T> traitClass)
	{
		Trait.verifyValidTraitClass(traitClass);
		return (Optional<T>) Optional.ofNullable(traits.get(traitClass));
	}
	
	public <T extends Trait> T phantomize(Class<T> traitClass)
	{
		Trait.verifyValidTraitClass(traitClass);
		return get(traitClass)
			.orElseGet(() -> PhantomTraitFactory.phantomize(this, traitClass));
	}
	
	public boolean remove(Class<? extends Trait> traitClass)
	{
		Trait.verifyValidTraitClass(traitClass);
		Trait removed = traits.remove(traitClass);
		return removed != null;
	}
	
	@Override
	public String toString()
	{
		return traits.values()
			.stream()
			.map(TraitLookup::toString)
			.collect(Collectors.joining("\n"));
	}
}
