package endercrypt.library.commons.trait.phantom;


import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import endercrypt.library.commons.trait.Trait;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


@Retention(RUNTIME)
@Target(TYPE)
public @interface Phantomizeable
{
	Class<? extends Trait> value();
}
