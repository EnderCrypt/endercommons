package endercrypt.library.commons.trait.phantom;


import endercrypt.library.commons.trait.Trait;
import endercrypt.library.commons.trait.TraitException;
import endercrypt.library.commons.trait.Traits;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


public class PhantomTraitFactory
{
	@SuppressWarnings("unchecked")
	public static <T extends Trait> T phantomize(Traits traits, Class<T> traitClass)
	{
		Trait.verifyValidTraitClass(traitClass);
		
		Phantomizeable phantomizeable = traitClass.getDeclaredAnnotation(Phantomizeable.class);
		if (phantomizeable == null)
		{
			throw new TraitException("Cannot construct phantom object of non-phantom class: " + traitClass);
		}
		Class<? extends Trait> phantomClass = phantomizeable.value();
		Trait phantom = constructPhantom(phantomClass);
		traits.getListeners().getInsert().getTrigger().onInsert(phantom);
		
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		Class<?>[] interfaces = new Class[] { traitClass };
		PhantomInvocationHandler phantomHandler = new PhantomInvocationHandler(traitClass, phantom);
		return (T) Proxy.newProxyInstance(classLoader, interfaces, phantomHandler);
	}
	
	private static Trait constructPhantom(Class<? extends Trait> phantomClass)
	{
		try
		{
			Constructor<? extends Trait> phantomConstructor = phantomClass.getConstructor();
			return phantomConstructor.newInstance();
		}
		catch (ReflectiveOperationException e)
		{
			throw new TraitException("Failed to construct phantom class " + phantomClass, e);
		}
	}
	
	@RequiredArgsConstructor
	@Getter
	private static class PhantomInvocationHandler implements InvocationHandler
	{
		@NonNull
		private final Class<?> traitClass;
		
		@NonNull
		private final Trait phantom;
		
		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
		{
			return method.invoke(phantom, args);
		}
	}
}
