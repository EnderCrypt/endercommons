package endercrypt.library.commons.trait;

@SuppressWarnings("serial")
public class TraitException extends RuntimeException
{
	public TraitException(String message)
	{
		super(message);
	}
	
	public TraitException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
