package endercrypt.library.commons.trait;


import java.util.HashMap;
import java.util.Map;


public class TraitLookup
{
	private static Map<Class<?>, Class<?>> traitClasses = new HashMap<>();
	
	public static synchronized Class<?> getTraitClass(Class<?> targetClass)
	{
		return traitClasses.computeIfAbsent(targetClass, TraitLookup::lookup);
	}
	
	private static Class<?> lookup(Class<?> targetClass)
	{
		return Trait.findTraitClass(targetClass)
			.orElseThrow(() -> new TraitException("Failed to lookup trait class of " + targetClass));
	}
	
	public static String toString(Trait trait)
	{
		Class<?> traitClass = getTraitClass(trait.getClass());
		return traitClass.getSimpleName() + ":" + trait.toString();
	}
}
