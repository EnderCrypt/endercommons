package endercrypt.library.commons.extended.graphics;


import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;


public class SmartImageGraphics extends SmartGraphics
{
	public static SmartImageGraphics fromFile(String file) throws IOException
	{
		return fromFile(Paths.get(file));
	}
	
	public static SmartImageGraphics fromFile(Path file) throws IOException
	{
		return fromFile(file.toFile());
	}
	
	public static SmartImageGraphics fromFile(File file) throws IOException
	{
		return new SmartImageGraphics(ImageIO.read(file));
	}
	
	public static SmartImageGraphics fromBytes(byte[] bytes) throws IOException
	{
		return new SmartImageGraphics(ImageIO.read(new ByteArrayInputStream(bytes)));
	}
	
	private final BufferedImage image;
	
	public SmartImageGraphics(BufferedImage image)
	{
		super(image.createGraphics());
		this.image = image;
	}
	
	// GETTERS //
	
	public BufferedImage getImage()
	{
		return image;
	}
	
	public void disposeImage()
	{
		getImage().flush();
	}
	
	public void disposeAll()
	{
		dispose();
		disposeImage();
	}
	
	public byte[] toImageBytes(String format)
	{
		ByteArrayOutputStream output = toOutputStream(format);
		return output.toByteArray();
	}
	
	public ByteArrayOutputStream toOutputStream(String format)
	{
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try
		{
			ImageIO.write(getImage(), format, output);
		}
		catch (IOException e)
		{
			throw new AssertionError(); // cant happen, no IO activity
		}
		return output;
	}
	
	public ByteArrayInputStream toInputStream(String format)
	{
		return new ByteArrayInputStream(toOutputStream(format).toByteArray());
	}
	
	public int getWidth()
	{
		return getImage().getWidth();
	}
	
	public int getHeight()
	{
		return getImage().getHeight();
	}
	
	public void drawGrid(int spacing)
	{
		for (int x = 0; x < getWidth(); x += spacing)
		{
			drawLine(x, 0, x, getHeight());
		}
		
		for (int y = 0; y < getHeight(); y += spacing)
		{
			drawLine(0, y, getWidth(), y);
		}
	}
	
	public void clear()
	{
		fillRect(0, 0, getWidth(), getHeight());
	}
	
	public void clear(Color color)
	{
		Color currentColor = getColor();
		setColor(color);
		clear();
		setColor(currentColor);
	}
}
