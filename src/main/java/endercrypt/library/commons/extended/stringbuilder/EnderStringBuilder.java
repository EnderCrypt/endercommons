package endercrypt.library.commons.extended.stringbuilder;

public class EnderStringBuilder extends DelegateStringBuilder
{
	public EnderStringBuilder()
	{
		super();
	}
	
	public EnderStringBuilder(CharSequence seq)
	{
		super(seq);
	}
	
	public EnderStringBuilder(int capacity)
	{
		super(capacity);
	}
	
	public EnderStringBuilder(String str)
	{
		super(str);
	}
	
	public EnderStringBuilder newline()
	{
		append('\n');
		return this;
	}
	
	public void repeat(int count)
	{
		ensureCapacity((length() * count) + 16);
		append(toString().repeat(count));
	}
	
	public String clear()
	{
		String text = toString();
		setLength(0);
		return text;
	}
	
	public void stripTailingNewlines()
	{
		while (length() > 0 && charAt(length() - 1) == '\n')
		{
			setLength(length() - 1);
		}
	}
}
