package endercrypt.library.commons.reject.exceptions;

import endercrypt.library.commons.reject.RejectionException;

public class NotImplementedException extends RejectionException
{
	private static final long serialVersionUID = 4898309326930898497L;
	
	/**
	 * 
	 */
	
	public NotImplementedException()
	{
		super();
	}
}
