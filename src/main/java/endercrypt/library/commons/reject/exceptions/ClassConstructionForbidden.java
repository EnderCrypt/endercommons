package endercrypt.library.commons.reject.exceptions;

import endercrypt.library.commons.reject.RejectionException;

public class ClassConstructionForbidden extends RejectionException
{
	private static final long serialVersionUID = -5739211020186002904L;
	
	/**
	 * 
	 */
	
	public ClassConstructionForbidden()
	{
		super("cannot construct static class");
	}
}
