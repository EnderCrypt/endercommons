package endercrypt.library.commons.reject;

import endercrypt.library.commons.reject.exceptions.ClassConstructionForbidden;
import endercrypt.library.commons.reject.exceptions.NotImplementedException;

public class ThrowRejectDueTo
{
	private ThrowRejectDueTo()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
	
	public static void forbiddenConstructionAttempt()
	{
		throw new ClassConstructionForbidden();
	}
	
	public static void notYetImplemented()
	{
		throw new NotImplementedException();
	}
}
