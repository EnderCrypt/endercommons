package endercrypt.library.commons.reject;

public abstract class RejectionException extends RuntimeException
{
	private static final long serialVersionUID = -3930544925686841273L;
	
	/**
	 * 
	 */
	
	public RejectionException()
	{
		super();
	}
	
	public RejectionException(String message)
	{
		super(message);
	}
}
