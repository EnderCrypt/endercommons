package endercrypt.library.commons;

public class EnderCommonsException extends RuntimeException
{
	private static final long serialVersionUID = -8325521106918505636L;
	
	public EnderCommonsException(String message)
	{
		super(message);
	}
	
	public EnderCommonsException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
