package endercrypt.library.commons.journal.stream;


import endercrypt.library.commons.journal.JournalIo;
import endercrypt.library.commons.journal.metadata.JournalMetadataIncrements;
import endercrypt.library.commons.journal.paths.JournalPaths;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import lombok.NonNull;


public class JournalFileOutputStream extends OutputStream
{
	private final JournalPaths paths;
	private final OutputStream bufferOutputStream;
	private final JournalMetadataIncrements increments = new JournalMetadataIncrements();
	
	public JournalFileOutputStream(@NonNull JournalPaths paths) throws FileNotFoundException
	{
		this.paths = paths;
		this.bufferOutputStream = new FileOutputStream(paths.getBufferPath().toFile());
	}
	
	@Override
	public void write(int b) throws IOException
	{
		bufferOutputStream.write(b);
		increments.feed(b);
	}
	
	@Override
	public void close() throws IOException
	{
		bufferOutputStream.close();
		increments.modified();
		increments.write(paths);
		JournalIo.synchronize(paths);
	}
}
