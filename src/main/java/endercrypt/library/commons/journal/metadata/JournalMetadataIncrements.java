package endercrypt.library.commons.journal.metadata;


import endercrypt.library.commons.journal.paths.JournalPaths;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.CRC32;

import lombok.Getter;


public class JournalMetadataIncrements
{
	@Getter
	private long size = 0;
	
	private final CRC32 crc32 = new CRC32();
	
	@Getter
	private long modifiedTime;
	
	public void feed(int b)
	{
		size++;
		crc32.update(b);
		modified();
	}
	
	public void modified()
	{
		modifiedTime = System.currentTimeMillis();
	}
	
	public long getCrc32()
	{
		return crc32.getValue();
	}
	
	public JournalMetadata write(JournalPaths paths) throws FileNotFoundException, IOException
	{
		JournalMetadata metadata = new JournalMetadata(this);
		metadata.write(paths);
		return metadata;
	}
}
