package endercrypt.library.commons.journal.metadata;


import endercrypt.library.commons.journal.paths.JournalPaths;


public class InvalidJournalMetadata extends Exception
{
	private static final long serialVersionUID = -8800295149053526615L;
	
	public InvalidJournalMetadata(JournalPaths path)
	{
		super(path.getMetadataPath().toString());
	}
}
