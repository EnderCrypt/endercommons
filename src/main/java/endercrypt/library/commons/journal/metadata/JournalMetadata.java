package endercrypt.library.commons.journal.metadata;


import static endercrypt.library.commons.interfaces.EqualsCheck.*;
import static java.util.function.Predicate.*;

import endercrypt.library.commons.data.DataSource;
import endercrypt.library.commons.journal.paths.JournalPaths;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.util.Optional;
import java.util.function.Predicate;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Getter
public class JournalMetadata
{
	private static final String SIGNATURE = "EnderCrypt.Journal.Sync";
	private static final Predicate<String> signatureChecker = not(stringPredicate(SIGNATURE));
	
	public static JournalMetadata write(JournalPaths path, long size, long crc32, long modifiedTime) throws FileNotFoundException, IOException
	{
		JournalMetadata metadata = new JournalMetadata(size, crc32, modifiedTime);
		metadata.write(path);
		return metadata;
	}
	
	public static Optional<JournalMetadata> optionalRead(JournalPaths paths)
	{
		try
		{
			return Optional.of(read(paths));
		}
		catch (InvalidJournalMetadata e)
		{
			return Optional.empty();
		}
	}
	
	public static JournalMetadata read(JournalPaths paths) throws InvalidJournalMetadata
	{
		try (DataInputStream input = new DataInputStream(new BufferedInputStream(new FileInputStream(paths.getMetadataPath().toFile()))))
		{
			String signature = input.readUTF();
			if (signatureChecker.test(signature))
			{
				throw new InvalidJournalMetadata(paths);
			}
			long size = input.readLong();
			long crc32 = input.readLong();
			long modifiedTime = input.readLong();
			return new JournalMetadata(size, crc32, modifiedTime);
		}
		catch (IOException e)
		{
			throw new InvalidJournalMetadata(paths);
		}
	}
	
	public static boolean check(JournalPaths paths) throws IOException
	{
		JournalMetadata metadata = JournalMetadata.optionalRead(paths).orElse(null);
		if (metadata == null)
		{
			return false;
		}
		
		long bufferSize = Files.size(paths.getBufferPath());
		if (metadata.getSize() != bufferSize)
		{
			return false;
		}
		
		/*
		 * reading the whole file, only to copy it later is very inefficient
		 * however it definetly adds that extra layer of safety, especially
		 * after a crash
		 */
		long bufferCrc32 = DataSource.DISK.read(paths.getBufferPath()).crc32();
		if (metadata.getCrc32() != bufferCrc32)
		{
			return false;
		}
		
		return true;
	}
	
	private final long size;
	private final long crc32;
	private final long modifiedTime;
	
	protected JournalMetadata(JournalMetadataIncrements increments)
	{
		size = increments.getSize();
		crc32 = increments.getCrc32();
		modifiedTime = increments.getModifiedTime();
	}
	
	public FileTime getModifiedFileTime()
	{
		return FileTime.fromMillis(getModifiedTime());
	}
	
	public void write(JournalPaths paths) throws FileNotFoundException, IOException
	{
		try (DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(paths.getMetadataPath().toFile()))))
		{
			output.writeUTF(SIGNATURE);
			output.writeLong(size);
			output.writeLong(crc32);
			output.writeLong(modifiedTime);
		}
	}
}
