package endercrypt.library.commons.journal.paths;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import lombok.Getter;


@Getter
public class JournalPaths
{
	private final Path master;
	private final String filename;
	
	private final Path bufferPath;
	private final Path metadataPath;
	
	public JournalPaths(Path master)
	{
		this.master = master;
		this.filename = master.getFileName().toString();
		this.bufferPath = master.getParent().resolve("." + filename + ".buffer.journal");
		this.metadataPath = master.getParent().resolve("." + filename + ".metadata.journal");
	}
	
	public void deleteMaster() throws IOException
	{
		Files.deleteIfExists(getMaster());
	}
	
	public void deleteSlaves() throws IOException
	{
		Files.deleteIfExists(getBufferPath());
		Files.deleteIfExists(getMetadataPath());
	}
	
	public boolean isSlavesAvailable()
	{
		return Files.exists(getBufferPath()) && Files.exists(getMetadataPath());
	}
}
