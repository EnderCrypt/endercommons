package endercrypt.library.commons.journal;


import endercrypt.library.commons.journal.metadata.JournalMetadata;
import endercrypt.library.commons.journal.paths.JournalPaths;
import endercrypt.library.commons.journal.stream.JournalFileOutputStream;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.AtomicMoveNotSupportedException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;


/**
 * 
 * provides safe read/writes, with guarantee of not becoming corrupted, even
 * with replacing files including during os panic or power outage
 * 
 * <p>
 * vanilla:<br/>
 * a read = 1 read<br/>
 * a write = 1 write
 * 
 * with no finished buffer:<br/>
 * a read = 1 read<br/>
 * a write = 1 read, 1 write
 * 
 * with a finished but not transfered buffer:<br/>
 * a read = 2 read<br/>
 * a write = 2 read, 1 write
 * </p>
 * 
 * by "1" it is meant having to go through the file completely, reading/writting
 * all bytes due to the heavy amount of i/o its advice to avoid using these
 * (counts assume successful atomic move)
 * 
 */
public class JournalIo
{
	public static boolean synchronize(Path path) throws IOException
	{
		return synchronize(new JournalPaths(path));
	}
	
	public static boolean synchronize(JournalPaths paths) throws IOException
	{
		if (paths.isSlavesAvailable() == false)
		{
			paths.deleteSlaves();
			return false;
		}
		
		JournalMetadata metadata = JournalMetadata.optionalRead(paths).orElse(null);
		if (metadata == null)
		{
			paths.deleteSlaves();
			return false;
		}
		
		try
		{
			Files.move(paths.getBufferPath(), paths.getMaster(), StandardCopyOption.ATOMIC_MOVE, StandardCopyOption.REPLACE_EXISTING);
		}
		catch (AtomicMoveNotSupportedException e)
		{
			Files.copy(paths.getBufferPath(), paths.getMaster(), StandardCopyOption.REPLACE_EXISTING);
		}
		
		Files.setLastModifiedTime(paths.getMaster(), metadata.getModifiedFileTime());
		
		paths.deleteSlaves();
		return true;
	}
	
	public static InputStream read(Path path) throws IOException
	{
		synchronize(path);
		return new FileInputStream(path.toFile());
	}
	
	public static byte[] readBytes(Path path) throws IOException
	{
		try (BufferedInputStream input = new BufferedInputStream(read(path)))
		{
			return input.readAllBytes();
		}
	}
	
	public static OutputStream write(Path path) throws IOException
	{
		JournalPaths paths = new JournalPaths(path);
		synchronize(path);
		return new JournalFileOutputStream(paths);
	}
	
	public static void writeBytes(Path path, byte[] bytes) throws IOException
	{
		try (BufferedOutputStream output = new BufferedOutputStream(write(path)))
		{
			output.write(bytes);
		}
	}
}
