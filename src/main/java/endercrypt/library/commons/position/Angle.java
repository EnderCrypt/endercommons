package endercrypt.library.commons.position;


import endercrypt.library.commons.misc.Ender;

import java.io.Serializable;


public class Angle implements Serializable
{
	private static final long serialVersionUID = -7669496080994851082L;
	
	/**
	 * 
	 */
	
	public static Angle random()
	{
		return fromRadians(Circle.random());
	}
	
	public static Angle zero()
	{
		return fromRadians(0.0);
	}
	
	public static Angle copy(Angle angle)
	{
		return new Angle()
			.setAngle(angle);
	}
	
	public static Angle fromRadians(double value)
	{
		return new Angle()
			.setRadians(value);
	}
	
	public static Angle fromDegrees(double value)
	{
		return new Angle()
			.setDegrees(value);
	}
	
	private double radians = 0.0;
	
	private Angle()
	{
		// requires call from static methods
	}
	
	// ADD //
	
	public Angle addAngle(Angle angle)
	{
		addRadians(angle.getRadians());
		return this;
	}
	
	public Angle addRadians(double value)
	{
		setRadians(getRadians() + value);
		return this;
	}
	
	public Angle addDegrees(double value)
	{
		setDegrees(getDegrees() + value);
		return this;
	}
	
	// SET //
	
	public Angle setAngle(Angle angle)
	{
		setRadians(angle.getRadians());
		return this;
	}
	
	public Angle setRadians(double value)
	{
		radians = Ender.math.floorMod(value, Circle.FULL);
		return this;
	}
	
	public Angle setDegrees(double value)
	{
		setRadians(Math.toRadians(value));
		return this;
	}
	
	// SPECIAL //
	
	public Angle flip()
	{
		addRadians(Circle.HALF);
		return this;
	}
	
	public Angle negative()
	{
		setRadians(-getRadians());
		return this;
	}
	
	// GET //
	
	public double getRadians()
	{
		return radians;
	}
	
	public double getDegrees()
	{
		return Math.toDegrees(getRadians());
	}
	
	@Override
	public String toString()
	{
		return Ender.math.round(getDegrees(), 2) + String.valueOf('\u00B0');
	}
}
