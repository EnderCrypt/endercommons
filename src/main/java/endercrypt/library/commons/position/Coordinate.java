package endercrypt.library.commons.position;


import java.io.Serializable;
import java.util.function.IntUnaryOperator;


public class Coordinate implements Serializable
{
	private static final long serialVersionUID = -3020715036566828080L;
	
	public static final Coordinate ZERO = new Coordinate(0, 0);
	
	/**
	 * 
	 */
	
	private final int x;
	private final int y;
	
	public Coordinate(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	// GETTER
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	// MAP
	
	public Coordinate map(IntUnaryOperator xOperator, IntUnaryOperator yOperator)
	{
		return new Coordinate(xOperator.applyAsInt(x), yOperator.applyAsInt(y));
	}
	
	// ADD
	
	public Coordinate add(Coordinate other)
	{
		return add(other.x, other.y);
	}
	
	public Coordinate add(int x, int y)
	{
		return map(xv -> xv + x, yv -> yv + y);
	}
	
	// SUB
	
	public Coordinate sub(Coordinate other)
	{
		return sub(other.x, other.y);
	}
	
	public Coordinate sub(int x, int y)
	{
		return map(xv -> xv - x, yv -> yv - y);
	}
	
	// OFFSET
	
	private Vector asVectorOffset(Coordinate other)
	{
		return Vector.offset(new Vector(x, y), new Vector(other.x, other.y));
	}
	
	// DIRECTION
	
	public double directionTo(Coordinate other)
	{
		return asVectorOffset(other).getAngle();
	}
	
	// DISTANCE
	
	public double distanceTo(Coordinate other)
	{
		return asVectorOffset(other).getLength();
	}
	
	// OVERRIDES
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof Coordinate)) return false;
		Coordinate other = (Coordinate) obj;
		if (x != other.x) return false;
		if (y != other.y) return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		return Coordinate.class.getSimpleName() + " [x=" + getX() + ", y=" + getY() + "]";
	}
}
