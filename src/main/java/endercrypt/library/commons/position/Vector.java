package endercrypt.library.commons.position;


import endercrypt.library.commons.misc.Ender;

import java.io.Serializable;


public class Vector implements Serializable
{
	private static final long serialVersionUID = 7945939804528999442L;
	
	/**
	 * 
	 */
	
	public static Vector copy(Vector other)
	{
		return new Vector(other.x, other.y);
	}
	
	public static Vector byAngle(double angle)
	{
		double x = Math.cos(angle);
		double y = Math.sin(angle);
		return new Vector(x, y);
	}
	
	public static Vector byAngle(double angle, double length)
	{
		Vector vector = byAngle(angle);
		vector.multiply(length);
		return vector;
	}
	
	public static Vector offset(Vector vector1, Vector vector2)
	{
		Vector copy = Vector.copy(vector1);
		copy.subtract(vector2);
		return copy;
	}
	
	public static Vector random()
	{
		return byAngle(Circle.random());
	}
	
	public static Vector random(double length)
	{
		return byAngle(Circle.random(), length);
	}
	
	public double x;
	public double y;
	
	// CONSTRUCTORS //
	
	public Vector(int x, int y)
	{
		this((double) x, (double) y);
	}
	
	public Vector(double x, double y)
	{
		set(x, y);
	}
	
	// SET //
	
	public void set(Vector vector)
	{
		set(vector.x, vector.y);
	}
	
	public void set(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	
	// ADD
	
	public void add(Vector vector)
	{
		add(vector.x, vector.y);
	}
	
	public void add(double x, double y)
	{
		set(this.x + x, this.y + y);
	}
	
	// SUBTRACT
	
	public void subtract(Vector vector)
	{
		subtract(vector.x, vector.y);
	}
	
	public void subtract(double x, double y)
	{
		set(this.x - x, this.y - y);
	}
	
	// MULTIPLY
	
	public void multiply(Vector vector)
	{
		multiply(vector.x, vector.y);
	}
	
	public void multiply(double length)
	{
		multiply(length, length);
	}
	
	public void multiply(double x, double y)
	{
		set(this.x * x, this.y * y);
	}
	
	// DIVIDE
	
	public void divide(Vector vector)
	{
		divide(vector.x, vector.y);
	}
	
	public void divide(double length)
	{
		multiply(length, length);
	}
	
	public void divide(double x, double y)
	{
		set(this.x / x, this.y / y);
	}
	
	// ANGLE
	
	public double getAngle()
	{
		return Math.atan2(y, x);
	}
	
	public double getAngle(Vector other)
	{
		return Vector.offset(other, this).getAngle();
	}
	
	public void addAngle(double angle)
	{
		setAngle(getAngle() + angle);
	}
	
	public void setAngle(double direction)
	{
		set(Vector.byAngle(direction, getLength()));
	}
	
	public double getAngleDifference(Vector other)
	{
		return Ender.math.angleDifference(getAngle(), other.getAngle());
	}
	
	// LENGTH
	
	public double getLength()
	{
		return Math.sqrt((x * x) + (y * y));
	}
	
	public double getLength(Vector other)
	{
		return Vector.offset(this, other).getLength();
	}
	
	public double getLengthFromPerspective(double angle)
	{
		Vector copy = Vector.copy(this);
		copy.addAngle(-(getAngle() * 2) + angle);
		return copy.x;
	}
	
	public void normalize()
	{
		if (x == 0 && y == 0)
		{
			return;
		}
		if (Double.isNaN(x) || Double.isNaN(y))
		{
			throw new IllegalStateException("cant normalize NaN (" + this + ")");
		}
		boolean xInf = Double.isInfinite(x);
		boolean yInf = Double.isInfinite(y);
		if (xInf || yInf)
		{
			x = xInf ? Math.signum(x) : 0;
			y = yInf ? Math.signum(y) : 0;
			return;
		}
		double max = Math.max(Math.abs(x), Math.abs(y));
		x /= max;
		y /= max;
	}
	
	public void setLength(double length)
	{
		set(Vector.byAngle(getAngle(), length));
	}
	
	public void truncate(double length)
	{
		setLength(Math.min(getLength(), length));
	}
	
	// MISC
	
	public void flip()
	{
		set(-x, -y);
	}
	
	// OVERRIDES
	
	@Override
	public String toString()
	{
		return Vector.class.getSimpleName() + "[x=" + Ender.math.round(x, 5) + ", y=" + Ender.math.round(y, 5) + " | angle=" + Ender.math.round(Math.toDegrees(getAngle()), 1) + "°, length=" + Ender.math.round(getLength(), 5) + "]";
	}
}
