package endercrypt.library.commons.position;


import endercrypt.library.commons.reject.ThrowRejectDueTo;


public class Circle
{
	public static final double HALF = Math.PI;
	public static final double QUARTER = HALF / 2;
	public static final double PACMAN = HALF + QUARTER; // fraction: 3/4
	public static final double FULL = HALF * 2;
	
	public static final double RIGHT = FULL;
	public static final double UP = QUARTER;
	public static final double LEFT = HALF;
	public static final double DOWN = PACMAN;
	
	private Circle()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
	
	public static double random()
	{
		return Math.random() * Circle.FULL;
	}
}
