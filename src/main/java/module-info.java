
/**
 * @author endercrypt
 */
module EnderCommons
{
	requires static lombok;
	requires transitive java.desktop;
	requires java.compiler;
	
	exports endercrypt.library.commons;
	exports endercrypt.library.commons.data;
	exports endercrypt.library.commons.debug.sysout;
	exports endercrypt.library.commons.data.extra;
	exports endercrypt.library.commons.entry;
	exports endercrypt.library.commons.extended.graphics;
	exports endercrypt.library.commons.extended.stringbuilder;
	exports endercrypt.library.commons.interfaces;
	exports endercrypt.library.commons.iterator;
	exports endercrypt.library.commons.journal;
	exports endercrypt.library.commons.listener;
	exports endercrypt.library.commons.journal.paths;
	exports endercrypt.library.commons.misc;
	exports endercrypt.library.commons.misc.array;
	exports endercrypt.library.commons.misc.clazz;
	exports endercrypt.library.commons.misc.collector;
	exports endercrypt.library.commons.misc.collector.collectors.adapter;
	exports endercrypt.library.commons.misc.collector.collectors.color;
	exports endercrypt.library.commons.misc.collector.collectors.entry;
	exports endercrypt.library.commons.misc.color;
	exports endercrypt.library.commons.misc.exception;
	exports endercrypt.library.commons.misc.files;
	exports endercrypt.library.commons.misc.format;
	exports endercrypt.library.commons.misc.generics;
	exports endercrypt.library.commons.misc.generics.hierarchy;
	exports endercrypt.library.commons.misc.graphics;
	exports endercrypt.library.commons.misc.loop;
	exports endercrypt.library.commons.misc.math;
	exports endercrypt.library.commons.misc.parse;
	exports endercrypt.library.commons.misc.stream;
	exports endercrypt.library.commons.misc.text;
	exports endercrypt.library.commons.misc.thread;
	exports endercrypt.library.commons.misc.time;
	exports endercrypt.library.commons.position;
	exports endercrypt.library.commons.pubimm;
	exports endercrypt.library.commons.randomizer;
	exports endercrypt.library.commons.reject;
	exports endercrypt.library.commons.structs.number;
	exports endercrypt.library.commons.structs.range;
	exports endercrypt.library.commons.structs.sign;
	exports endercrypt.library.commons.trait;
	exports endercrypt.library.commons.trait.listener;
	exports endercrypt.library.commons.trait.phantom;
	exports endercrypt.library.commons.tricks;
}
